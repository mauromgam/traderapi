<?php
require_once __DIR__ . "/../Db/Db.php";
require_once __DIR__ . "/../Utils/Utils.php";

class SmsAlert
{
    protected $apiKey;
    protected $apiSecret;
    protected $pair;
    protected $to;
    protected $db;
    protected $message;
    protected $url = 'https://rest.nexmo.com/sms/json';

    /**
     * smsAlert constructor.
     * @param $apiKey
     * @param $apiSecret
     * @param $pair
     * @param $to
     * @param Db $db
     */
    public function __construct($apiKey, $apiSecret, $pair, $to, Db $db)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->pair = $pair;
        $this->to = $to;
        $this->db = $db;
        $this->message = "Test";
    }

    /**
     * @param mixed $pair
     */
    public function setPair($pair)
    {
        $this->pair = $pair;
    }

    /**
     * Send SMS alert
     *
     * @param $date
     * @param $trend
     * @param $bbandsValue
     * @return mixed curl -X "POST" "https://rest.nexmo.com/sms/json" \
     */
    public function sendAlert($date, $trend, $bbandsValue)
    {
        $result = false;
        $this->message = "A bband squeeze was detected on " . $this->pair . ". SMS not sent.";
        if (false && !$this->checkSentMessagesLast2Hours()) {
            $this->message = "A bband squeeze was detected on " . $this->pair;
            $result = $this->sendSMS();
        }

        $this->logSmsMessages($date, $trend, $bbandsValue);

        return $result;
    }

    /**
     * Send Customised SMS alert
     *
     * @param $date
     * @param $message
     *
     * @return mixed curl -X "POST" "https://rest.nexmo.com/sms/json" \
     */
    public function sendCustomAlert($date, $message)
    {
        $result = false;
        $this->message = $message . " " . $this->pair;
        if (!$this->checkSentMessagesLast2Hours()) {
            $result = $this->sendSMS();
        } else {
            $this->message .= ' SMS not sent';
        }

        $this->logSmsMessages($date, null, 0);

        return $result;
    }

    /**
     * @return bool
     */
    private function checkSentMessagesLast2Hours()
    {
        $sql = "SELECT * FROM sms_alerts 
              WHERE 
                date >= CURRENT_TIMESTAMP - INTERVAL 2 HOUR 
                AND text_message_sent = 1";
        if ($this->message) {
            $sql .= " AND text_message = '{$this->message}'";
        }
        $sql .= " LIMIT 1;";
        $smsAlerts = $this->db->execute($sql);

        return $smsAlerts->num_rows > 0;
    }

    /**
     * @param $date
     * @param $trend
     * @param $bbandsValue
     * @return bool|mysqli_result
     */
    private function logSmsMessages($date, $trend, $bbandsValue)
    {
        $textMessageSent = 0;
        if (strpos($this->message, "SMS not sent") === false) {
            $textMessageSent = 1;
        }

        $sql = "INSERT IGNORE INTO sms_alerts 
            (`market`, `date`, `to`, `text_message`, `text_message_sent`, `sar_trend`, `bbands_squeeze_value`) 
            VALUES ('{$this->pair}', '$date', '{$this->to}', '{$this->message}', $textMessageSent, '$trend', $bbandsValue);";
        $smsAlert = $this->db->execute($sql);

        return $smsAlert;
    }

    /**
     * curl -X "POST" "https://rest.nexmo.com/sms/json" \
     * -d "from=Acme Inc" \
     * -d "text=A text message sent using the Nexmo SMS API" \
     * -d "to=TO_NUMBER" \
     * -d "api_key=NEXMO_API_KEY" \
     * -d "api_secret=NEXMO_API_SECRET"
     */
    public function sendSMS()
    {
        $fields = array(
            'from' => urlencode('tradeMonitor'),
            'text' => urlencode($this->message),
            'to' => urlencode($this->to),
            'api_key' => urlencode($this->apiKey),
            'api_secret' => urlencode($this->apiSecret)
        );

        //url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        $fields_string = rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }


}