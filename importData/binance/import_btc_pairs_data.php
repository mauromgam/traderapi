<?php
require_once __DIR__ . '/../../Db/Db.php';
require_once __DIR__ . '/../../Utils/Messages.php';
require_once __DIR__ . '/../../coinAnalytics/ChartData.php';
require_once __DIR__ . '/../../config/config.php';
require_once __DIR__ . '/../../vendor/autoload.php';

$startProcess = microtime(true);

$key = $botConfig['binanceApiKey'];
$apisecret = $botConfig['binanceApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tpolApiKey\n";
    echo "\tpolApiSecret\n";
    echo "../../config/config.php\n\n";
    exit(-1);
}

echo "Starting... \n";

$period = '5m';
$intervalSpec = isset($argv[1]) ? $argv[1] : 'PT2H';
$third = (int)(isset($argv[2]) ? $argv[2] : 1);

if ($third === 3) {
    echo "The script will run only the third part of the third of the array of currencies.\n";
} elseif ($third === 2) {
    echo "The script will run only the second part of the third of the array of currencies.\n";
} else {
    echo "The script will run only the first part of the third of the array of currencies.\n";
}
echo "Available commands:\n";
echo "\t{$argv[0]} PT15M\n";
echo "\t{$argv[0]} PT15M 2\n";
echo "\t{$argv[0]} PT15M 3\n\n";

$api = new Binance\API($key, $apisecret);
$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$chartData = new ChartData($db);

echo Messages::GETTING_BTC_PAIRS;
$currencies = $api->getBTCPairs();

// split currencies in half so it can
// processed quicker
$total = count($currencies);
$thirdValue = round($total/3);

if ($third === 3) {
    $currencies = array_slice($currencies, $thirdValue * 2, $total);
} elseif ($third === 2) {
    $currencies = array_slice($currencies, $thirdValue, $thirdValue);
} else {
    $currencies = array_slice($currencies, 0, $thirdValue);
}


// delete data older than 12 months
//$chartData->deleteOldData(12, 'chart_data_binance');

// 2018-03-03 10:55:00
//$count = 60;
//while ($count > 0) {
    foreach ($currencies as $pair) {

        // P3DT11H
//        $days = $count * 3;
//        $hours = $count * 11;
//        $intervalSpec = 'P' . $days . 'DT' . $hours . 'H';

        $chartData->setPair($pair);

        $now = date('Y-m-d H:i:s');
        $date = new DateTime($now);
        $date->sub(new DateInterval($intervalSpec));
        $start = date_format($date, 'Y-m-d H:i:s');
        $data = $api->candlesticks($pair, $period, null, strtotime($start) * 1000, strtotime($now) * 1000);

        if (isset($data['error'])) {
            continue;
        }

        echo "Importing... ($now)\n";

        // prepare insert
        $insert = $chartData->getPrepareInsertSql('chart_data_binance');

        foreach ($data as $key => $datum) {

            $date = date('Y-m-d H:i:s', (int)$datum['openTime'] / 1000);
            $open = sprintf("%.8f", $datum['open']);
            $high = sprintf("%.8f", $datum['high']);
            $low = sprintf("%.8f", $datum['low']);
            $close = sprintf("%.8f", $datum['close']);
            $volume = sprintf("%.8f", $datum['volume']);

            $query = "('$pair', '$period', '$date', 
            $high, $low, $open, $close,
            $volume)";

            $query = $insert . $query;
            $retVal = $db->execute($query);

            if (!$retVal) {
                Utils::errorLog('Could not enter data: ' . mysqli_error($db->getActiveConnection()));
            } else {
                echo "Data inserted for $date - $pair\n";
            }

        }

        echo "$pair\n";

//        sleep(1);

    }
//    $count--;
//}
$endProcess = microtime(true);
echo sprintf("Script finished. Time: %.8f.\n\n", $endProcess - $startProcess);
