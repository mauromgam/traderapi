<?php

require_once __DIR__ . '/../../APIs/APIPoloniex.php';
require_once __DIR__ . '/../../config/config.php';

$startProcess = microtime(true);

if(count($argv) != 2){
    echo "usage ".$argv[0]." FROM_DATE\n";
    echo "\texample: ".$argv[0]." 2000-01-01\n";
    exit(-1);
}

$FROM_DATE = new DateTime($argv[1]);
$DATABASE = "poloniex";
$TABLE = "user_trade_history_poloniex";

echo "Database: ". $DATABASE;
echo "\n";
echo "Table: ". $TABLE;
echo "\n";


$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];

$DATABASE_HOST = $botConfig['host'];
$DATABASE_USER = $botConfig['db_user'];
$DATABASE_PASS = $botConfig['db_pass'];
$DATABASE_DB = $botConfig['db'];
if(empty($DATABASE_HOST)){
    echo "Set environment variables from database:\n";
    echo "export DATABASE_HOST=\"localhost\"\n";
    echo "export DATABASE_USER=\"your user\"\n";
    echo "export DATABASE_PASS=\"your password\"\n";
    exit(-1);
}

echo "Connecting... \n\n";
$conn = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_DB);
if (!$conn) {
    die('Could not connect: ' . mysqli_error($conn));
}
echo "Connected successfully... \n\n";

$api = new APIPoloniex($key, $apisecret);

$currencies = $api->get_btc_pairs();

foreach ($currencies as $pair) {

    usleep(500000);

    echo "Market: $pair\n";
    echo "Downloading user trade history...\n";

    $start = $FROM_DATE->getTimestamp();
    $end = time();
    $result = $api->get_my_trade_history($pair, $start, $end);

    if (isset($result['error'])) {
        echo "An error has occurred: {$result['error']}\n\n";
        continue;
    }

    if (empty($result)) {
        echo "Empty data.\n\n";
        continue;
    }

    echo "\n\t`market`, `globalTradeID`, `tradeID`, `date`, `rate`, `amount`, `total`, `fee`, `orderNumber`, `type`, `category`\n";
    $values_array = array();
    foreach ($result as $k => $value) {
        $values_array[] = "('" . $pair . "', " . $value['globalTradeID'] . ", " . $value['tradeID'] . ", '" . $value['date'] . "', " . sprintf("%.8f", $value['rate']) . ", " . sprintf("%.8f", $value['amount']) . ", " . sprintf("%.8f", $value['total']) . ", " . sprintf("%.8f", $value['fee']) . ", " . $value['orderNumber'] . ", '" . $value['type'] . "', '" . $value['category'] . "')";
    }

    print_r($values_array);

    /* insert ignoring duplicated entries */
    $sql = 'INSERT IGNORE INTO ' . $TABLE . ' ' .
        '(`market`, `globalTradeID`, `tradeID`, `date`, `rate`, `amount`, `total`, `fee`, `orderNumber`, `type`, `category`) ' .
        'VALUES ' . join(",", $values_array);

    $return = mysqli_query($conn, $sql);
    if (!$return) {

        echo mysqli_error($conn) . "\n";

    } else {

        echo "Entered data successfully for $pair\n\n";

    }
}


$endProcess = microtime(true);
echo sprintf("Script finished. Time: %.8f\n\n", $endProcess - $startProcess);
mysqli_close($conn);