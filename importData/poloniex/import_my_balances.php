<?php

require_once __DIR__ . '/../../APIs/APIPoloniex.php';
require_once __DIR__ . '/../../config/config.php';

function print_r2($val){
    print_r($val);
}

if (count($argv) != 2) {
    echo "usage ".$argv[0]." FROM_DATE\n";
    echo "\texample: ".$argv[0]." 2000-01-01\n";
    exit(-1);
}

$FROM_DATE = new DateTime($argv[1]);
$DATABASE = "poloniex";
$TABLE = "user_balances_poloniex";


echo "Database: ". $DATABASE;
echo "\n";
echo "Table: ". $TABLE;
echo "\n";

$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set environment variables from Exchange API Key:\n";
    echo "export POL_KEY=\"your key\"\n";
    echo "export POL_PASS=\"your super big secret\"\n";
    echo "\n\nPlease be careful with this sensible data. Store your keys encrypted, protect your profile and allow only connections from your IP and do not allow withdrawals.\n";
    exit(-1);
}

$DATABASE_HOST = $botConfig['host'];
$DATABASE_USER = $botConfig['db_user'];
$DATABASE_PASS = $botConfig['db_pass'];
$DATABASE_DB = $botConfig['db'];
if(empty($DATABASE_HOST)){
    echo "Set environment variables from database:\n";
    echo "export DATABASE_HOST=\"localhost\"\n";
    echo "export DATABASE_USER=\"your user\"\n";
    echo "export DATABASE_PASS=\"your password\"\n";
    exit(-1);
}

echo "connecting... \n\n";
$conn =  mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_DB);
if (!$conn) {
    die('Could not connect: ' . mysqli_error($conn));
}
echo "Connected successfully... \n\n";

/**
 * PRIVATE API
 */
$api = new APIPoloniex($key, $apisecret);
$start = $FROM_DATE->getTimestamp();
$end = time();
$result = $api->get_complete_balances();

// Create SQL:
$values_array = array();
foreach($result as $coin => $value){
    if((double)$value['btcValue'] > 0.0) {
        $values_array[] = "('" . $coin . "', " . (double)$value['available'] . ", " . (double)$value['onOrders'] . ", " . (double)$value['btcValue'] . ")";
    }
}

mysqli_select_db($conn, $DATABASE);

/* truncate table */
$sql = "TRUNCATE TABLE $TABLE";
$retval = mysqli_query($conn, $sql);
if(! $retval ) {
    echo mysqli_error($conn)."\n";
}

/* insert ignoring duplicated entries */
$sql = 'INSERT INTO '.$TABLE.' '.
    '(coin, available, onOrders, btcValue) '.
    'VALUES '. join(",", $values_array);

$retval = mysqli_query($conn, $sql);
if(! $retval ) {
    echo mysqli_error($conn)."\n";
}

echo "Entered data successfully\n\n";

mysqli_close($conn);