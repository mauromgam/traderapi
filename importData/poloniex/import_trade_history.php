<?php

require_once __DIR__ . '/../../APIs/APIPoloniex.php';
require_once __DIR__ . '/../../config/config.php';

$startProcess = microtime(true);

/**
*   Show last 10 records
*/
function get_last_orders($conn, $TABLE, $pair){
    echo "Show last 10 records:\n";
    $sql = "SELECT `date`, `type`, `rate`, `amount`, `total` FROM $TABLE 
            WHERE market='".$pair."' ORDER BY $TABLE.date DESC LIMIT 10;";
    echo $sql;
    echo "\n\n";
    $result = mysqli_query($conn, $sql);

    if(! $result ) {
      echo mysqli_error($conn)."\n";
    }

    printf("date\t\t\t|\ttype\t\t|\trate\t\t|\tamount\t\t|\ttotal\n");
    printf("====================================================================================================================\n");
    while ($fila = mysqli_fetch_array($result, MYSQLI_NUM)) {
        printf("%s\t|\t%s\t\t|\t%.8f\t|\t%.8f\t|\t%.8f\n", $fila[0], $fila[1], $fila[2], $fila[3], $fila[4]);  
    }
}

/**
*   Show total order of sell and buy by MONTH and HOUR
*/
function get_orders_by_hour($conn, $TABLE, $pair){
    echo "\n\n";
    echo "Show total order of sell and buy by HOUR\n";
    $sql = "SELECT MONTHNAME(date) monthname, HOUR(date) hour, 'sell' as type, COUNT(type) as total_orders, sum(total) as volume_btc, AVG(`rate`) as avg_rate, DAY(`date`) as day, `date` FROM `".$TABLE."` WHERE market='".$pair."' AND type='sell' GROUP BY MONTH(date), DAY(date), HOUR(date)
    UNION
    SELECT MONTHNAME(date) monthname, HOUR(date) hour, 'buy' as type , COUNT(type) as total_orders, sum(total) as volume_btc, AVG(`rate`) as avg_rate, DAY(`date`) as day, `date` FROM `".$TABLE."` WHERE market='".$pair."' AND type='buy' GROUP BY MONTH(date), DAY(date), HOUR(date) 
   ORDER BY `date` DESC, `type` LIMIT 4;";
    echo $sql;
    echo "\n\n";
    $result = mysqli_query($conn, $sql);

    if(! $result ) {
      echo mysqli_error($conn)."\n";
    }

    printf("month\t\t|\thour\t|\ttype\t\t|\ttotal orders\t|\tBTC volume\t|\tAVG rate\n");
    printf("====================================================================================================================================\n");
    while ($fila = mysqli_fetch_array($result, MYSQLI_NUM)) {
        printf("%s\t\t|\t%d\t|\t%s\t\t|\t%d\t\t|\t%.8f\t|\t%.8f\n", $fila[0], $fila[1], $fila[2], $fila[3], $fila[4], $fila[5]);  
    }
}


/**
*   Show total order of sell and buy by MONTH, HOUR and each $minute minutes
*/
function get_by_minute($conn, $TABLE, $pair, $minute = 4, $LIMIT = 5){
    $SECONDS = $minute * 60;
    echo "\n\n";
    echo "Show total order of sell and buy by $minute minutes\n";
    $sql = "SELECT MONTHNAME(date) month, HOUR(date) hour, MINUTE(date) as minute, 'sell' as type, COUNT(type) as total_orders, sum(total) as volume_btc, AVG(`rate`) as avg_rate, DAY(`date`) as day FROM `".$TABLE."` WHERE market='".$pair."' AND type='sell' GROUP BY MONTH(date), DAY(date), HOUR(date), UNIX_TIMESTAMP(date) DIV $SECONDS
    UNION
    SELECT MONTHNAME(date) month, HOUR(date) hour, MINUTE(date) as minute, 'buy' as type , COUNT(type) as total_orders, sum(total) as volume_btc, AVG(`rate`) as avg_rate, DAY(`date`) as day FROM `".$TABLE."` WHERE market='".$pair."' AND type='buy' GROUP BY MONTH(date), HOUR(date), UNIX_TIMESTAMP(date) DIV $SECONDS ORDER BY month, day DESC, hour DESC, minute DESC, type LIMIT $LIMIT;";
    echo $sql;
    echo "\n\n";
    $result = mysqli_query($conn, $sql);

    if(! $result ) {
      echo mysqli_error($conn)."\n";
    }

    printf("month\t\t|\thour\t|\tminute\t|\ttype\t|\ttotal orders\t|\tBTC volume\t|\tAVG rate\n");
    printf("===================================================================================================================================\n");
    while ($fila = mysqli_fetch_array($result, MYSQLI_NUM)) {
        printf("%s\t\t|\t%d\t|\t%d\t|\t%s\t|\t%d\t\t|\t%.8f\t|\t%.8f\n", $fila[0], $fila[1], $fila[2], $fila[3], $fila[4], $fila[5], $fila[6]);  
    }
}

/**
*   Show total order of sell and buy by MONTH
*/
function get_orders_by_month($conn, $TABLE, $pair){
    echo "\n";
    echo "Show total order of sell and buy by MONTH\n";
    $sql = "SELECT MONTHNAME(date) month, 'sell' as type, COUNT(type) as total_orders, sum(total) as volume_btc FROM `".$TABLE."` WHERE market='".$pair."' AND type='sell' GROUP BY MONTH(date) UNION
    SELECT MONTHNAME(date) month, 'buy' as type , COUNT(type) as total_orders, sum(total) as volume_btc FROM `".$TABLE."` WHERE market='".$pair."' AND type='buy' GROUP BY MONTH(date) ORDER BY month, type;
    ;";
    echo $sql;
    echo "\n\n";
    $result = mysqli_query($conn, $sql);

    if(! $result ) {
      echo mysqli_error($conn)."\n";
    }

    printf("month\t\t|\ttype\t\t|\ttotal orders\t|\tBTC volume\n");
    printf("===========================================================================================================\n");
    while ($fila = mysqli_fetch_array($result, MYSQLI_NUM)) {
        printf("%s\t\t|\t%s\t\t|\t%d\t\t|\t%.8f\n", $fila[0], $fila[1], $fila[2], $fila[3]);  
    }
}

$DATABASE = "poloniex";
$TABLE = "trade_history_poloniex";
echo "Database: ". $DATABASE;
echo "\n";
echo "Table: ". $TABLE;
echo "\n";


$DATABASE_HOST = $botConfig['host'];
$DATABASE_USER = $botConfig['db_user'];
$DATABASE_PASS = $botConfig['db_pass'];
$DATABASE_DB = $botConfig['db'];
if(empty($DATABASE_HOST)){
    echo "Set environment variables from database:\n";
    echo "export DATABASE_HOST=\"localhost\"\n";
    echo "export DATABASE_USER=\"your user\"\n";
    echo "export DATABASE_PASS=\"your password\"\n";
    exit(-1);
}

echo "connecting... \n\n";
$conn =  mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_DB);
if (!$conn) {
    die('Could not connect: ' . mysqli_error($conn));
}
echo "Connected successfully... \n\n";

$api = new APIPoloniex("", "");

$currencies = $api->get_btc_pairs();

foreach ($currencies as $pair) {
    usleep(500000);

    echo "Market: $pair\n";
    echo "Downloading general trade history...\n";
    $result = $api->get_trade_history($pair);

    if (isset($result['error'])) {
        echo "An error has occurred: {$result['error']}\n\n";
        continue;
    }

    if (empty($result)) {
        echo "Empty data.\n\n";
        continue;
    }

    $values_array = array();
    foreach($result as $k => $value){
        $values_array[] = "('".$pair."', ".$value['globalTradeID'].", ".$value['tradeID'].", '" . $value['date'] . "', '".$value['type']."', " . (double)$value['rate'] .", ". (double)$value['amount'] .", ". (double)$value['total'] .")";
    }

    /* insert ignoring duplicated entries */
    $sql = 'INSERT IGNORE INTO '.$TABLE.' '.
          '(`market`, `globalTradeID`, `tradeID`, `date`, `type`, `rate`, `amount`, `total`) '.
          'VALUES '. join(",", $values_array);

    $return = mysqli_query($conn, $sql);

    if(! $return ) {
        echo mysqli_error($conn)."\n";
    }

    echo "Entered data successfully for $pair\n\n";

    //echo "\n";
    //get_orders_by_hour($conn, $TABLE, $pair);

    //echo "\n";
    //get_last_orders($conn, $TABLE, $pair);
    
    /* show by last 5 $MINUTE minutes */
    //get_by_minute($conn, $TABLE, $pair, 5, 10);

}

$endProcess = microtime(true);
echo sprintf("Script finished. Time: %.8f\n\n", $endProcess - $startProcess);
mysqli_close($conn);