<?php

require_once __DIR__ . '/../../APIs/APIPoloniex.php';
require_once __DIR__ . '/../../config/config.php';

$startProcess = microtime(true);

$DATABASE = "poloniex";
$TABLE = "user_open_orders_poloniex";

echo "Database: ". $DATABASE;
echo "\n";
echo "Table: ". $TABLE;
echo "\n";


$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set environment variables from Exchange API Key:\n";
    echo "export POL_KEY=\"your key\"\n";
    echo "export POL_PASS=\"your super big secret\"\n";
    echo "\n\nPlease be careful with this sensible data. Store your keys encrypted, protect your profile and allow only connections from your IP and do not allow withdrawals.\n";
    exit(-1);
}

$DATABASE_HOST = $botConfig['host'];
$DATABASE_USER = $botConfig['db_user'];
$DATABASE_PASS = $botConfig['db_pass'];
$DATABASE_DB = $botConfig['db'];
if(empty($DATABASE_HOST)){
    echo "Set environment variables from database:\n";
    echo "export DATABASE_HOST=\"localhost\"\n";
    echo "export DATABASE_USER=\"your user\"\n";
    echo "export DATABASE_PASS=\"your password\"\n";
    exit(-1);
}

echo "Connecting... \n\n";
$conn =  mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_DB);
if (!$conn) {
    die('Could not connect: ' . mysqli_error($conn));
}
echo "Connected successfully... \n\n";


/**
 * PRIVATE API
 */
$api = new APIPoloniex($key, $apisecret);

$currencies = $api->get_btc_pairs();

foreach ($currencies as $pair) {

    usleep(500000);

    echo "Market: $pair\n";
    echo "Downloading user open orders...\n";
    $result = $api->get_open_orders($pair);

    if (isset($result['error'])) {
        echo "An error has occurred: {$result['error']}\n\n";
        continue;
    }

    if (empty($result)) {
        echo "Empty data.\n\n";
        continue;
    }

    $values_array = array();
    foreach ($result as $k => $value) {
        $values_array[] = "('" . $pair . "', " . $value['orderNumber'] . ", '" . $value['type'] . "', " . sprintf("%.8f", $value['rate']) . ", " . sprintf("%.8f", $value['amount']) . ", " . sprintf("%.8f", $value['total']) . ", '" . $value['date'] . "', " . $value['margin'] . ")";
    }

    print_r($values_array);

    /* insert ignoring duplicated entries */
    $sql = 'INSERT IGNORE INTO ' . $TABLE . ' ' .
        '(`market`, `orderNumber`, `type`, `rate`, `amount`, `total`, `date`, `margin`) ' .
        'VALUES ' . join(",", $values_array);

    $return = mysqli_query($conn, $sql);
    if (!$return) {
        echo mysqli_error($conn) . "\n\n";
    }

    echo "Entered data successfully for $pair\n\n";
}

$endProcess = microtime(true);
echo sprintf("Script finished. Time: %.8f\n\n", $endProcess - $startProcess);
mysqli_close($conn);