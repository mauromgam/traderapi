<?php
require_once __DIR__ . '/../../APIs/APIPoloniex.php';
require_once __DIR__ . '/../../Db/Db.php';
require_once __DIR__ . '/../../coinAnalytics/ChartData.php';
require_once __DIR__ . '/../../config/config.php';

$startProcess = microtime(true);

$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tpolApiKey\n";
    echo "\tpolApiSecret\n";
    echo "../../config/config.php\n\n";
    exit(-1);
}

echo "Starting... \n";

$period = 300;
$intervalSpec = isset($argv[1]) ? $argv[1] : 'PT2H';
$third = (int)(isset($argv[2]) ? $argv[2] : 1);

if ($third === 3) {
    echo "The script will run only the third part of the third of the array of currencies.\n";
} elseif ($third === 2) {
    echo "The script will run only the second part of the third of the array of currencies.\n";
} else {
    echo "The script will run only the first part of the third of the array of currencies.\n";
}
echo "Available commands:\n";
echo "\t{$argv[0]} PT15M\n";
echo "\t{$argv[0]} PT15M 2\n";
echo "\t{$argv[0]} PT15M 3\n\n";

$api = new APIPoloniex($key, $apisecret);
$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$chartData = new ChartData($db);

$currencies = $api->get_btc_pairs();

// split currencies in half so it can
// processed quicker
$total = count($currencies);
$thirdValue = round($total/3);

if ($third === 3) {
    $currencies = array_slice($currencies, ($thirdValue * 2) - 1, $total);
} elseif ($third === 2) {
    $currencies = array_slice($currencies, $thirdValue - 1, $thirdValue + 1);
} else {
    $currencies = array_slice($currencies, 0, $thirdValue + 1);
}


// delete data older than 12 months
$chartData->deleteOldData(12);

foreach ($currencies as $pair) {

    $chartData->setPair($pair);

    $now = date('Y-m-d H:i:s');
    $date = new DateTime($now);
    $date->sub(new DateInterval($intervalSpec));
    $start = date_format($date, 'Y-m-d H:i:s');
    $data = $api->get_chart_data($pair, $period, strtotime($start), strtotime($now));

    if (isset($data['error'])) {
        continue;
    }

    echo "Importing... ($now)\n";

    // prepare insert
    $insert = $chartData->getPrepareInsertSql();

    foreach ($data as $key => $datum) {

        $date = date('Y-m-d H:i:s', $datum['date']);
        $high = sprintf("%.8f", $datum['high']);
        $low = sprintf("%.8f", $datum['low']);
        $open = sprintf("%.8f", $datum['open']);
        $close = sprintf("%.8f", $datum['close']);
        $volume = $datum['volume'];
        $quoteVolume = $datum['quoteVolume'];
        $weightedAverage = sprintf("%.8f", $datum['weightedAverage']);

        $query = "('$pair', $period, '$date', 
            $high, $low, $open, $close,
            $volume, $quoteVolume, $weightedAverage)";

        $query = $insert . $query;
        $retVal = $db->execute($query);

        if (!$retVal) {
            Utils::errorLog('Could not enter data: ' . mysqli_error($db->getActiveConnection()));
        } else {
            echo "Data inserted for $date - $pair\n";
        }

    }

    echo "$pair\n";

//        sleep(1);

}

$endProcess = microtime(true);
echo sprintf("Script finished. Time: %.8f.\n\n", $endProcess - $startProcess);
