<?php
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/../Utils/Messages.php';

class ImportFunctions
{
    protected $db;

    /**
     * ImportFunctions constructor.
     * @param Db $db
     */
    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    /**
     * @param string $symbol
     * @param array $chartData
     * @param string $interval
     * @return bool
     */
    public function importIntradayChartData($symbol, $chartData, $interval)
    {
        $this->getChartData($chartData);
        if (empty($chartData)) {
            echo sprintf(Messages::IMPORT_FUNCTION_NO_DATA, $symbol);

            return false;
        }

        echo sprintf(Messages::IMPORT_FUNCTION_START, 'Chart', $symbol);

        $sql = "INSERT IGNORE INTO `alpha_vantage_chart_data` (`function`, `symbol`, `period`, `date`, `high`, `low`, `open`, `close`, `volume`) VALUES ";

        $count = 1;
        foreach ($chartData as $date => $chartDatum) {
            $open = (double)$chartDatum['1. open'];
            $high = (double)$chartDatum['2. high'];
            $low = (double)$chartDatum['3. low'];
            $close = (double)$chartDatum['4. close'];
            $volume = (double)$chartDatum['5. volume'];

            $query[] = "('TIME_SERIES_INTRADAY', '$symbol', '$interval', '$date', '$high', '$low', '$open', '$close', '$volume')";

            if ($count % 400 == 0) {
                $query = $sql . implode(',', $query);
                $this->db->execute($query);
                $query = [];
            }

            $count++;
        }

        if (!empty($query)) {
            $query = $sql . implode(',', $query) . ";";
            $this->db->execute($query);
        }

        echo sprintf(Messages::IMPORT_FUNCTION_END, 'Chart', $symbol);

        return true;
    }

    private function getChartData(&$chartData)
    {
        if (isset($chartData['Time Series (60min)'])) {

            $chartData = $chartData['Time Series (60min)'];

        } elseif (isset($chartData['Time Series (30min)'])) {

            $chartData = $chartData['Time Series (30min)'];

        } elseif (isset($chartData['Time Series (15min)'])) {

            $chartData = $chartData['Time Series (15min)'];

        } elseif (isset($chartData['Time Series (5min)'])) {

            $chartData = $chartData['Time Series (5min)'];

        } elseif (isset($chartData['Time Series (1min)'])) {

            $chartData = $chartData['Time Series (1min)'];

        } else {

            $chartData = false;

        }
    }

    /**
     * @param $symbol
     * @param $rsiData
     * @param $interval
     * @return bool
     */
    public function importRsiData($symbol, $rsiData, $interval)
    {
        if (!$this->dataExist($rsiData, 'Technical Analysis: RSI', $symbol)) {
            return false;
        }
        $rsiData = $rsiData['Technical Analysis: RSI'];

        echo sprintf(Messages::IMPORT_FUNCTION_START, 'RSI', $symbol);

        $sql = "INSERT IGNORE INTO alpha_vantage_stocks_indicators (`symbol`, `function`, `date`, `value1`, `interval`) VALUES ";

        $count = 1;
        foreach ($rsiData as $date => $rsiDatum) {
            $value = (double)reset($rsiDatum);

            $query[] = "('$symbol', 'RSI', '$date:00', $value, '$interval')";

            if ($count % 400 == 0) {
                $query = $sql . implode(',', $query);
                $this->db->execute($query);
                $query = [];
            }

            $count++;
        }

        if (!empty($query)) {
            $query = $sql . implode(',', $query) . ";";
            $this->db->execute($query);
        }

        echo sprintf(Messages::IMPORT_FUNCTION_END, 'RSI', $symbol);

        return true;
    }

    /**
     * @param $symbol
     * @param $aroonData
     * @param $interval
     * @return bool
     */
    public function importAroonData($symbol, $aroonData, $interval)
    {
        if (!$this->dataExist($aroonData, 'Technical Analysis: AROON', $symbol)) {
            return false;
        }
        $aroonData = $aroonData['Technical Analysis: AROON'];

        echo sprintf(Messages::IMPORT_FUNCTION_START, 'AROON', $symbol);

        $sql = "INSERT IGNORE INTO alpha_vantage_stocks_indicators (`symbol`, `function`, `date`, `value1`, `value2`, `interval`) VALUES ";

        $count = 1;
        foreach ($aroonData as $date => $aroonDatum) {
            $aroonUp = (double)$aroonDatum['Aroon Up'];
            $aroonDown = (double)$aroonDatum['Aroon Down'];

            $query[] = "('$symbol', 'AROON', '$date:00', $aroonUp, $aroonDown, '$interval')";

            if ($count % 400 == 0) {
                $query = $sql . implode(',', $query);
                $this->db->execute($query);
                $query = [];
            }

            $count++;
        }

        if (!empty($query)) {
            $query = $sql . implode(',', $query) . ";";
            $this->db->execute($query);
        }

        echo sprintf(Messages::IMPORT_FUNCTION_END, 'AROON', $symbol);

        return true;
    }

    /**
     * @param $data
     * @param $key
     * @param $symbol
     * @return bool
     */
    private function dataExist($data, $key, $symbol)
    {
        if (!isset($data[$key])) {
            echo sprintf(Messages::IMPORT_FUNCTION_NO_DATA2, $key, $symbol);

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getEtoroStocksArray()
    {
        return [
            'AABA',
            'ADBE',
            'ADP',
            'ADSK',
            'AKAM',
            'AMAT',
            'AMD',
            'AMS.MC',
            'AMX',
            'ANSS',
            'APH',
            'ARRS',
            'ATHM.CH',
            'ATVI',
            'BIDU',
            'BITA',
            'BT.L',
            'CAP.PA',
            'CAVM',
            'CDW',
            'CERN',
            'CHKP',
            'CHL',
            'COMM',
            'CRM',
            'CSCO',
            'CTL',
            'CTSH',
            'CYBR',
            'DNB',
            'EA',
            'FB',
            'FEYE',
            'FIS',
            'FSLR',
            'FTR',
            'GDDY',
            'GIB',
            'GLW',
            'GOOG',
            'GRMN',
            'GSAT',
            'HAWK',
            'HPQ',
            'HUBS',
            'IBM',
            'IDR.MC',
            'IMPV',
            'INFY',
            'INTC',
            'INTU',
            'IPGP',
            'JBL',
            'JD.CH',
            'JMEI.CH',
            'JNPR',
            'LDOS',
            'LRCX',
            'MSFT',
            'MSI',
            'MU',
            'MXL',
            'NATI',
            'NCR',
            'NEWR',
            'NLSN',
            'NOK',
            'NTAP',
            'NTES',
            'NUAN',
            'NVDA',
            'NXPI',
            'ORCL',
            'PANW',
            'POWI',
            'PYPL',
            'QCOM',
            'RENN',
            'RHT',
            'S',
            'SANM',
            'SBAC',
            'SCMN.ZU',
            'SHOP',
            'SNAP',
            'ST',
            'STM.MI',
            'SWKS',
            'SYMC',
            'SYNA',
            'SYNT',
            'T',
            'TEF',
            'TEO',
            'TRMB',
            'TWTR',
            'TXN',
            'UBNT',
            'VIAV',
            'VMW',
            'VOD',
            'VRSN',
            'VZ',
            'WDC',
            'WUBA',
            'XRX',
            'YELP',
            'YNDX',
            'ZEN',
            'ZNGA'
        ];
    }

    /**
     * @return array
     */
    public function getEtoroStocksServicesArray()
    {
        return [
            'AAL',
            'AAP',
            'ABE.MC',
            'ABG',
            'ABM',
            'AC.PA',
            'ACM',
            'ACN',
            'ADEN.ZU',
            'AES',
            'AGK.L',
            'AHT.L',
            'AIR.PA',
            'ALK',
            'ALLE',
            'AMZN',
            'AN',
            'ANF',
            'AON',
            'ARII',
            'ARMK',
            'ARW',
            'ASNA',
            'AVT',
            'AXE',
            'AZO',
            'BAB.L',
            'BABA',
            'BAH',
            'BBBY',
            'BBY',
            'BIG',
            'BKS',
            'BLMN',
            'BNZL.L',
            'BRBY.L',
            'BURL',
            'CAR',
            'CASY',
            'CHDN',
            'CHRW',
            'CHTR',
            'CMG',
            'CORE',
            'CPG.L',
            'CPI.L',
            'CSX',
            'CTAS',
            'CTRP.CH',
            'CZR',
            'DAL',
            'DDS',
            'DIS',
            'DISCA',
            'DISH',
            'DKS',
            'DPW.DS',
            'DPZ',
            'DRI',
            'EAT',
            'EBAY',
            'ESND',
            'EXPD',
            'EXPE',
            'EXPN.L',
            'EZJ.L',
            'FDX',
            'FISV',
            'FLT',
            'GFS.L',
            'GHC',
            'GLP',
            'GME',
            'GNC',
            'GPC',
            'GPI',
            'GPS',
            'GWW',
            'H',
            'HAIN',
            'HD',
            'HDS',
            'HLT',
            'HRI',
            'HSIC',
            'HTZ',
            'IAG.L',
            'IPG',
            'ITRK.L',
            'ITV.L',
            'JBHT',
            'JBLU',
            'JCP',
            'JCP',
            'JEC',
            'JOBS.CH',
            'JWN',
            'KBR',
            'KGF.L',
            'KMX',
            'KNX',
            'KSS',
            'LB',
            'LBTYA',
            'LBTYK',
            'LHA.DE',
            'LILA',
            'LMCA',
            'LSXMA',
            'LUV',
            'LVS',
            'LYV',
            'MAN',
            'MAR',
            'MCD',
            'MCO',
            'MELI',
            'MFS',
            'MGM',
            'MIK',
            'MKS.L',
            'MMYT',
            'MNST',
            'MRW.L',
            'MTCH',
            'MUSA',
            'NFLX',
            'NWSA',
            'NXT.L',
            'ODP',
            'OMC',
            'OMI',
            'ORA.PA',
            'ORLY',
            'OUT',
            'P',
            'PCLN',
            'PDCO',
            'PSON.L',
            'PUB.PA',
            'QSR',
            'R',
            'RAD',
            'REL.L',
            'RHI',
            'RMG.L',
            'RMS',
            'ROST',
            'RRD',
            'RUSHB',
            'SAH',
            'SAVE',
            'SBRY.L',
            'SBUX',
            'SCI',
            'SERV',
            'SHLD',
            'SIG',
            'SIX',
            'SKY.L',
            'SNX',
            'SPD.L',
            'SPGI',
            'SPTN',
            'SQ',
            'SVU',
            'SYY',
            'TA',
            'TECD',
            'TIF',
            'TIVO',
            'TJX',
            'TL5.MC',
            'TSCO.L',
            'TV',
            'TWX',
            'UAL',
            'UNFI',
            'UNIT',
            'URBN',
            'URI',
            'VIAB',
            'VIPS',
            'VRSK',
            'W',
            'WCC',
            'WLWT',
            'WMH.L',
            'WMT',
            'WPP.L',
            'WSM',
            'WSO',
            'WTB.L',
            'WYN',
            'WYNN',
            'YUM',
        ];
    }

    public function getEtoroSymbols($argv)
    {
        $arr1 = $this->getEtoroStocksArray();
        $arr2 = $this->getEtoroStocksServicesArray();
        if (isset($argv[1], $argv[2]) && $argv[1] == 1 && $argv[2] == 1) {

            $stocksArray = array_splice($arr1, 0, 36);

        } elseif (isset($argv[1], $argv[2]) && $argv[1] == 1 && $argv[2] == 2) {

            $stocksArray = array_splice($arr1, 36, 36);

        } elseif (isset($argv[1], $argv[2]) && $argv[1] == 1 && $argv[2] == 3) {

            $stocksArray = array_splice($arr1, 72, count($arr1));

        } elseif (isset($argv[1], $argv[2]) && $argv[1] == 2 && $argv[2] == 2) {

            $stocksArray = array_splice($arr2, 36, 36);

        } elseif (isset($argv[1], $argv[2]) && $argv[1] == 2 && $argv[2] == 3) {

            $stocksArray = array_splice($arr2, 72, 36);

        } elseif (isset($argv[1], $argv[2]) && $argv[1] == 2 && $argv[2] == 4) {

            $stocksArray = array_splice($arr2, 108, 36);

        } elseif (isset($argv[1], $argv[2]) && $argv[1] == 2 && $argv[2] == 5) {

            $stocksArray = array_splice($arr2, 144, count($arr2));

        } else {

            $stocksArray = array_splice($arr2, 0, 36);

        }
        $etoroStocks = "'" . implode('\',\'', $stocksArray) . "'";

        return $etoroStocks;
    }


    /**
     * @param int $months
     */
    public function deleteOldData($months)
    {
        $sql = "DELETE FROM alpha_vantage_stocks_indicators WHERE `date` < CURRENT_TIMESTAMP - INTERVAL $months MONTH;";
        $this->db->execute($sql);
    }

}