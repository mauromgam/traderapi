<?php
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/../APIs/AlphaVantageAPI.php';
require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/ImportFunctions.php';
require_once __DIR__ . '/../Utils/Messages.php';

$now = date('d/m/Y H:i:s');
$startTime = microtime(true);
echo sprintf(Messages::IMPORT_SCRIPT_START, "Alpha Vantage", $now);

$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$alphaVantageApi = new AlphaVantageAPI($botConfig['alphaVantageApiKey'], $db);
$importFunctions = new ImportFunctions($db);

// delete data older than 6 months
$importFunctions->deleteOldData(6);
$etoroStocks = $importFunctions->getEtoroSymbols($argv);

$sql = "SELECT * FROM alpha_vantage_stock_symbols WHERE symbol IN ($etoroStocks);";
$stockSymbols = $db->execute($sql);


$i = 0;
//while($i < 1) {
    $interval = 'daily';
    foreach ($stockSymbols as $stockSymbol) {
        $symbol = $stockSymbol['symbol'];

        // This can be optional as the system is not doing any calculations with the chart data
        $intradayChartData = $alphaVantageApi->getIntradayChartData($symbol, $interval);
        $importFunctions->importIntradayChartData($symbol, $intradayChartData, $interval);

    }

    $now = date('d/m/Y H:i:s');
    $endTime = microtime(true);
    echo sprintf(Messages::IMPORT_SCRIPT_END, "Data - interval $interval", $now, date('H:i:s', $endTime - $startTime));
//}

