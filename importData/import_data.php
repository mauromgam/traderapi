<?php

require_once __DIR__ . '/../APIs/APIPoloniex.php';
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/../coinAnalytics/ChartData.php';
require_once __DIR__ . '/../config/config.php';

if(count($argv) < 3 || count($argv) > 4){
    echo "usage ".$argv[0]." MARKET period_in_seconds\n";
    echo "\texample: ".$argv[0]." USDT_BTC 1800\n";
    echo "\toptions:\n";
    echo "\t\tmarket: pairs of cryptocurrency underscore separated, like BTC_ETH.\n";
    echo "\t\tperiod_in_seconds: valid values are 60(Cryptowat.ch), 300, 900, 1800, 7200, 14400, and 86400\n";
    exit(-1);
}


$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tpolApiKey\n";
    echo "\tpolApiSecret\n";
    echo "../../config/config.php\n\n";
    exit(-1);
}

echo "Starting... \n";
echo "This script will be constantly retrieving data from Poloniex and storing on the database.\n\n";

$pair = $argv[1];
$period = $argv[2];
$start2YearsAgo = (isset($argv[3]) && $argv[3]) ? true : false;

$api = new APIPoloniex($key, $apisecret);
$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$chartData = new ChartData($db, $pair);

//for (;;) {

    $now = date('Y-m-d H:i:s');
    $date = new DateTime($now);
    if ($start2YearsAgo) {
        $date->sub(new DateInterval('P3M'));
    } else {
        $date->sub(new DateInterval('PT52H'));
    }
    $start = date_format($date, 'Y-m-d H:i:s');
    $data = $api->get_chart_data($pair, $period, strtotime($start), strtotime($now));

    echo "Importing... ($now)\n";

    // delete the last 2 hours of data so they can be inserted again with updated data
    $chartData->deleteLast2Hours($pair);

    // delete data older than 6 months
    $chartData->deleteOldData(6);

    // prepare insert
    $insert = $chartData->getPrepareInsertSql();

    foreach ($data as $key => $datum) {

        $date = date('Y-m-d H:i:s', $datum['date']);
        unset($datum['date']);
        $query = "('$pair', $period, '$date', " . implode(',', $datum) . ")";

        $query = $insert . $query;
        $retVal = $db->execute($query);

        if (!$retVal) {
            Utils::errorLog('Could not enter data: ' . mysqli_error($db->getActiveConnection()));
        } else {
            echo "Data inserted for $date\n";
        }

    }

    echo "$pair\n";
    sleep(60);

//}

echo "Script finished.\n\n";