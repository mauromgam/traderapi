<?php
require_once __DIR__ . '/../Utils/FileHelper.php';
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/../config/config.php';

echo "Importing Stock symbols...\n";

$filePath = __DIR__ . "/../Yahoo Ticker Symbols.csv";

if (!file_exists($filePath)) {
    echo "The file $filePath doesn't exist.";
    die;
}

$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$fileHelper = new FileHelper(
    $filePath,
    $db
);
$fileHelper->loadStockSymbolsIntoDatabase();


echo "Script finished.\n\n";