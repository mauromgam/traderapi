<?php
require_once __DIR__ . "/../Db/Db.php";

class AlphaVantageAPI
{
    protected $apiKey;
    protected $publicUrl = "https://www.alphavantage.co/query?";
    protected $db;

    public function __construct($apiKey, Db $db)
    {
        $this->apiKey = $apiKey;
        $this->publicUrl .= 'apikey=' . $this->apiKey . '&';
        $this->db = $db;
    }

    private function query(array $fields = array())
    {
        //url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        $fields_string = rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

//echo($this->publicUrl . $fields_string);die;


        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $this->publicUrl . $fields_string);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }

    /**
     * @param string $stockSymbol
     * @param string $interval
     * @param int $period
     * @param string $seriesType
     * @return mixed
     */
    public function getRSIData($stockSymbol, $interval = '30min', $period = 20, $seriesType = 'open')
    {
        $fields = [
            'function' => 'RSI',
            'symbol' => $stockSymbol,
            'interval' => $interval,
            'time_period' => $period,
            'series_type' => $seriesType
        ];

        return $this->getJsonDecodedData($fields, $stockSymbol);
    }

    /**
     * @param $stockSymbol
     * @param string $interval
     * @param int $period
     * @return mixed
     */
    public function getAroonData($stockSymbol, $interval = '30min', $period = 20)
    {
        $fields = [
            'function' => 'AROON',
            'symbol' => $stockSymbol,
            'interval' => $interval,
            'time_period' => $period
        ];

//var_dump($results);die;
        return $this->getJsonDecodedData($fields, $stockSymbol);
    }

    /**
     * @param $stockSymbol
     * @param string $interval
     * @param string $outputSize
     * @param string $dataType
     * @return mixed
     */
    public function getIntradayChartData($stockSymbol, $interval = '30min', $outputSize = 'full', $dataType = 'json')
    {
        $fields = [
            'function' => 'TIME_SERIES_INTRADAY',
            'symbol' => $stockSymbol,
            'interval' => $interval,
            'outputsize' => $outputSize,
            'datatype' => $dataType
        ];

        return $this->getJsonDecodedData($fields, $stockSymbol);
    }

    /**
     * @param $fields
     * @param $symbol
     * @return array
     */
    private function getJsonDecodedData($fields, $symbol)
    {
        echo sprintf(Messages::TRY_QUERY_DATA, $symbol);

        $retries = 0;
        $results = null;
        $error = false;
        while ($retries < 50) {
            $results = $this->query($fields);
            $results = json_decode($results, true);

            if (isset($results['Meta Data'])) {
                $error = false;
                break;
            } else {
                $error = true;
                echo Messages::QUERY_DATA_FAIL;
            }

            sleep(2);
            $retries++;
        }

        if ($error) {
            $interval = isset($fields['interval']) ? $fields['interval'] : '';
            $insert = "INSERT IGNORE INTO `alpha_vantage_stock_indicators_fail` (`function`, `symbol`, `period`) VALUES ";
            $insert .= "('{$fields['function']}', '{$fields['symbol']}', '$interval');";
            $this->db->execute($insert);
        }

        return $results;
    }

}
