<?php

class Db
{

//'host' => 'localhost',
//'db_user' => 'newspress',
//'db_pass' => 'newspress',
//'db' => 'poloniex',

    protected $host;
    protected $user;
    protected $pass;
    protected $db;
    protected $conn;

    /**
     * Db constructor.
     * @param $host
     * @param $user
     * @param $pass
     * @param $db
     */
    public function __construct($host, $user, $pass, $db)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->db = $db;
    }

    public function getActiveConnection()
    {
        if (!$this->conn) {
            $this->conn = mysqli_connect($this->host, $this->user, $this->pass, $this->db);
        }

        if (!$this->conn) {
            die('Could not connect: ' . mysqli_error($this->conn));
        }

        return$this->conn;
    }

    /**
     * @param $sql
     * @return bool|mysqli_result
     */
    public function execute($sql)
    {
        $results = mysqli_query($this->getActiveConnection(), $sql);

        if(! $results ) {
            die('Error: ' . mysqli_error($this->getActiveConnection()));
        }

        return $results;
    }

}