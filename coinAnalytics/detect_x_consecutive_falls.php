<?php
require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/ChartData.php';
require_once __DIR__ . '/ConsecutiveDowns.php';
require_once __DIR__ . '/../Utils/Messages.php';
require_once __DIR__ . '/../Utils/Utils.php';

$startProcess = microtime(true);



// TODO: create new server using PHP7.1+Apache+MariaDb
// #1 copy cron jobs from the other server
// #2 set cron jobs to copy all data between current time and P3DT11H ago
// #3 after #2, keep collecting the data between current time and PT15M ago
// #4 copy cron job to exec detect consecutive downs every 30 secs
// #5 scan_consecutive_falls shall run every 10 secs
//      - This script will look for downs on the database and open orders
//          in case a sequence is found



if(count($argv) < 3 || count($argv) > 6){
    echo "usage ".$argv[0]." consecutive_falls last_x_days max_x_hours exchange\n";
    echo "\texample: ".$argv[0]." 10 P8M 24 25 binance\n";
    echo "\toptions:\n";
    echo "\t\tnumber_of_downs: number of 5min periods going down consecutively.\n";
    echo "\t\tlast_x_days: number of days/months/years to go back in the past.\n";
    echo "\t\tx_hours: number of hours to start analysing the data after a position was opened (min 6).\n";
    echo "\t\tmax_x_hours: number of hours to analyse the data after a position was opened (min 7).\n";
    exit(-1);
}
$numberOfDowns = $argv[1];
$lastXDays = $argv[2];
$xHours = (isset($argv[3]) ? ($argv[3] < 6 ? 6 : $argv[3]) : 6);
$maxXHours = (isset($argv[4]) ? ($argv[4] < 7 ? 7 : $argv[4]) : 7);
$exchange = (isset($argv[5]) ? $argv[5] : 'binance');

echo "number_of_downs = $numberOfDowns.\n";
echo "last_x_days = $lastXDays.\n";
echo "x_hours = $xHours.\n";
echo "max_x_hours = $maxXHours.\n";
echo "exchange = $exchange.\n";

if ($xHours > $maxXHours) {
    echo "Error: x_hours can't be greater than max_x_hours.\n";
    exit(-1);
}

echo Messages::DETECT_CONSECUTIVE_FALLS_START;

$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$chartData = new ChartData($db);
$consecutiveDowns = new ConsecutiveDowns($db);
$exchangeTables = Utils::getExchangeTables($exchange);

//$consecutiveDowns->deleteOldData(12, $exchangeTables['consecutive_downs']);

$now = date('Y-m-d H:i:s');
$date = new DateTime($now);
$date->sub(new DateInterval($lastXDays));
$start = date_format($date, 'Y-m-d H:i:s');
$options = [
    'interval' => $exchangeTables['period'],
    'limit' => false,
    'date_from' => $start,
    'date_to' => $now,
];
$dataChart = $chartData->getChartData($options, $exchangeTables['chart_data'], true);
$dataChart = $dataChart['data'];

echo "start: $start\n";
echo "end: $now\n\n";

$sql = $consecutiveDowns->getPrepareInsertSql($exchangeTables['consecutive_downs']);

while ($xHours < $maxXHours) {

    $countDowns = 1;
    $previousValue = null;
    $highAfterXHours = 0;
    $highDateAfterXHours = null;
    $lowAfterXHours = 0;
    $lowDateAfterXHours = null;
    $closeAfterXHours = 0;
    $closeDateAfterXHours = null;
    $symbol = isset($dataChart[0]['market']) ? $dataChart[0]['market'] : null;
    $highestVolumeDateAftXHours = null;
    $highestVolumeAftXHours = 0;
    $sumVolumeGoingDown = 0;
    $percentageGoingDown = 0;
    $highestPercentage = 0;
    $lowestPercentage = 0;
    $date_ = null;

//    $closes = [];

    foreach ($dataChart as $key => $item) {

        $close = $item['close'];
        $market = $item['market'];
        $volume = $item['volume'];
        $period = $item['period'];
        $date = $item['date'];

        // If market symbol changes, reset important vars
        if ($symbol != $market) {
            $countDowns = 1;
            $previousValue = null;
            $sumVolumeGoingDown = 0;
            $percentageGoingDown = 0;
            echo sprintf(Messages::DETECT_CONSECUTIVE_FALLS_PARTIAL_END, $symbol);

//            $closes = [];
        }

        // If close value is going down, count +1
        // and store the highest volume going down
        if ($close < $previousValue && $previousValue !== null) {
            if ($countDowns == 1) {
                $percentageGoingDown = $close;
            }
            $sumVolumeGoingDown += $volume;
            $countDowns++;

//            $closes[] = [$close, $date];
        }

        // If current close value is higher than last one, reset counting
        if ($close > $previousValue && $previousValue !== null) {
            $countDowns = 1;
            $sumVolumeGoingDown = 0;
            $percentageGoingDown = 0;

//            $closes = [];
        }


        // if N consecutive downs, analyse the future data starting from the next 5min candle
        if ($countDowns > $numberOfDowns && $symbol == $market) {

//            echo "numberOfDowns: $numberOfDowns\n";
//            echo "date: $date\n";
//            echo "close: $close\n";
//            var_dump($closes);die;

            echo sprintf(Messages::DETECT_CONSECUTIVE_FALLS_FOUND, $numberOfDowns, $symbol);

            $percentageGoingDown = (($percentageGoingDown/$close)-1)*100;

            $countDowns = 1;
            $highAfterXHours = 0;
            $lowAfterXHours = 0;
            $highestVolumeDateAftXHours = 'NULL';
            $highestVolumeAftXHours = 0;
            $insert = null;

            // IF STILL ITERATING THE SAME COIN
            // CALCULATE THE HIGHEST AND LOWEST VALUES FOR THE NEXT X HOURS
            $key_ = $key + 1;
            $end = $key + ($xHours * 12); // X_HOURS * 12 is the number of periods in X_HOURS hours

            for (; $key_ < $end; $key_++) {

                // break if there's no future data or if coin changes
                if (!isset($dataChart[$key_]) || $dataChart[$key_]['market'] != $symbol) {
                    break;
                }

                // INITIALIZE FIELDS - START


                $nthDown = $key_ == $key + 1;
                // get the first open after the down
                if ($nthDown) {
                    $open_ = $dataChart[$key_]['open'];
                }

                $closeAfterXHours = $dataChart[$key_]['close'];
                $closeDateAfterXHours = $dataChart[$key_]['date'];

                $high_ = $dataChart[$key_]['high'];
                $low_ = $dataChart[$key_]['low'];
                $volume_ = $dataChart[$key_]['volume'];
                $date_ = $dataChart[$key_]['date'];
                if ($nthDown) {
                    $lowAfterXHours = $low_;
                    $lowDateAfterXHours = $date_;
                }
                // INITIALIZE FIELDS - END

                // SET HIGHEST AND LOWEST DATA - START
                if ($high_ > $highAfterXHours && !$nthDown) {
                    $highAfterXHours = $high_;
                    $highDateAfterXHours = $date_;
                }

                if ($low_ < $lowAfterXHours && !$nthDown) {
                    $lowAfterXHours = $low_;
                    $lowDateAfterXHours = $date_;
                }

                if ($volume_ > $highestVolumeAftXHours && !$nthDown) {
                    $highestVolumeAftXHours = $volume_;
                    $highestVolumeDateAftXHours = $dataChart[$key_]['date'];
                }

                if ($nthDown) {
                    $highestPercentage = 0;
                    $lowestPercentage = 0;
                } else {
                    $highestPercentage = ((($highAfterXHours / $open_) - 1) * 100);
                    $lowestPercentage = ((($lowAfterXHours / $open_) - 1) * 100);
                }
                // SET HIGHEST AND LOWEST DATA - END



                // SQL TO INSERT THE DATA FOR THE Nth DOWN
                // Analysed data is inserted after the loop finishes
                $sqlInsert = "('$market', '$period', '$date', 
                            $open_, $volume, $sumVolumeGoingDown, $percentageGoingDown,
                            $highAfterXHours, $lowAfterXHours, $closeAfterXHours, '$highDateAfterXHours', '$lowDateAfterXHours', '$closeDateAfterXHours', 
                            $highestPercentage, $lowestPercentage, 
                            $highestVolumeAftXHours, '$highestVolumeDateAftXHours', $numberOfDowns, $xHours)";
                $insert = $sql . $sqlInsert;

            }

            if (!empty($insert)) {
                $db->execute($insert);
            }

            $sumVolumeGoingDown = 0;
        }

        $symbol = $market;
        $previousValue = $close;
    }

    echo sprintf(Messages::DETECT_CONSECUTIVE_FALLS_END, date('d/m/Y H:i:s') . " ($xHours hours)");
    $xHours++;
}

$endProcess = microtime(true);
echo sprintf(Messages::DETECT_CONSECUTIVE_FALLS_TIME_TAKEN, $endProcess - $startProcess);
