<?php
/**
 * This class is used to manipulate the stocks indicators
 *
 * PHP Version 5.6^
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */

require_once __DIR__ . "/../Db/Db.php";

/**
 * Class Indicators
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */
class Indicators
{

    protected $db;
    protected $pair;
    protected $bbandsValues;

    /**
     * ChartData constructor.
     * @param Db $db
     * @param $pair
     */
    public function __construct(Db $db, $pair)
    {
        $this->db = $db;
        $this->pair = $pair;
    }

    /**
     * @return null|double
     */
    public function getBbandsValues()
    {
        if (!$this->bbandsValues) {
            $sql = "SELECT value FROM bband_squeeze WHERE market = '" . $this->pair . "'";
            $result = $this->db->execute($sql);

            foreach ($result as $item) {
                $this->bbandsValues = (double)$item['value'];
                break;
            }
        }

        return $this->bbandsValues;
    }

    public function getBbandsValuesArray($data, $closes, $period, $devup, $devdn)
    {
        $bbandsValues = [];
        foreach ($data as $key => $item) {
            $closesArray = Utils::getNPreviousCloses($period, $closes, $key);
            if (count($closesArray) > 1) {
                $bbandsValues[] = trader_bbands($closesArray, count($closesArray), $devup, $devdn, 0);
            }
        }

        return $bbandsValues;
    }

    /**
     * @param $bbandsValues
     * @return bool|mixed
     */
    public function bbandsSqueezeExists($bbandsValues)
    {
        $squeezeCalc = reset($bbandsValues[0]) - reset($bbandsValues[2]);
        if ($squeezeCalc < $this->getBbandsValues()) {
            return $squeezeCalc;
        }

        return false;
    }
}