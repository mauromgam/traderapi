<?php
/**
 * This class is used to manipulate the data from the chart_data table
 *
 * PHP Version 5.6^
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */

require_once __DIR__ . "/../Db/Db.php";

/**
 * Class ChartData
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */
class ChartData
{

    protected $db;
    protected $pair;

    /**
     * ChartData constructor.
     *
     * @param Db     $db
     * @param string $pair
     */
    public function __construct(Db $db, $pair = null)
    {
        $this->db = $db;
        $this->pair = $pair;
    }

    /**
     * @param string $pair
     *
     * @return void
     */
    public function setPair($pair)
    {
        $this->pair = $pair;
    }

    /**
     * @param array  $options
     * @param string $table
     * @param bool   $dataOnly
     *
     * @return array
     */
    public function getChartData(array $options, $table = 'chart_data', $dataOnly = false)
    {
        $sql[] = "SELECT * FROM $table";

        if (!empty($options)) {
            $sql[] = "WHERE";
        }

        if (!empty($options['market'])) {
            $where[] = "market = '{$options['market']}'";
        } else {
            $where[] = "market != 'BTCUSDT'";
        }

        if (!empty($options['interval']) && is_numeric($options['interval'])) {
            $where[] = "period = {$options['interval']}";
        } elseif (!empty($options['interval'])) {
            $where[] = "period = '{$options['interval']}'";
        }

        if (!empty($options['date_from']) && !empty($options['date_to'])) {
            $where[] = "date > '{$options['date_from']}'";
            $where[] = "date <= '{$options['date_to']}'";
        }

        if (!empty($where)) {
            $sql[] = implode(' AND ', $where);
        }

        if (!empty($options['order'])) {
            $sql[] = "ORDER BY {$options['order']}";
        } else {
            $sql[] = "ORDER BY market, date ASC";
        }

        if (!empty($options['limit'])) {
            $sql[] = "LIMIT " . $options['limit'] . ";";
        } elseif (!isset($options['limit']) || (isset($options['limit']) && $options['limit'])) {
            $sql[] = "LIMIT 2000;";
        }

        $dataChart = $this->db->execute(implode(' ', $sql));

        $highs = [];
        $lows = [];
        $closes = [];
        $data = [];
        foreach ($dataChart as $item) {
            if (!$dataOnly) {
                $highs[] = (double)$item['high'];
                $lows[] = (double)$item['low'];
                $closes[] = (double)$item['close'];
            }
            $data[] = $item;
        }

        return [
            'highs' => $highs,
            'lows' => $lows,
            'closes' => $closes,
            'data' => $data
        ];

    }

    /**
     * @param string $table
     *
     * @return string
     */
    public function getPrepareInsertSql($table = 'chart_data')
    {
        if ($table == 'chart_data') {
            return "INSERT IGNORE INTO $table 
                (market, period, date,
                high, low, open, close, 
                volume, quoteVolume, weightedAverage)
                VALUES ";
        } elseif ($table == 'chart_data_binance') {
            return "INSERT IGNORE INTO $table 
                (market, period, date,
                high, low, open, close, 
                volume)
                VALUES ";
        }

        return '';
    }

    /**
     * @param int    $months
     * @param string $table
     *
     * @return void
     */
    public function deleteOldData($months, $table = 'chart_data')
    {
        $sql = "DELETE FROM $table WHERE period = 300 AND date < CURRENT_TIMESTAMP - INTERVAL $months MONTH;";
        $this->db->execute($sql);
    }

    /**
     * @param string $pair
     * @param string $table
     *
     * @return void
     */
    public function deleteLast2Hours($pair, $table = 'chart_data')
    {
        $sql = "DELETE FROM $table WHERE market = '$pair' AND date >= CURRENT_TIMESTAMP - INTERVAL 2 HOUR;";
        $this->db->execute($sql);
    }

}