<?php
/**
 * This class is used to manipulate the data from the consecutive_downs table
 *
 * PHP Version 5.6^
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */

require_once __DIR__ . "/../Db/Db.php";

/**
 * Class ConsecutiveDowns
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */
class ConsecutiveDowns
{

    protected $db;
    protected $pair;

    /**
     * ChartData constructor.
     *
     * @param Db     $db
     * @param string $pair
     */
    public function __construct(Db $db, $pair = null)
    {
        $this->db = $db;
        $this->pair = $pair;
    }

    /**
     * @param string $pair
     *
     * @return void
     */
    public function setPair($pair)
    {
        $this->pair = $pair;
    }

    /**
     * @param array  $options
     * @param string $table
     * @param string $chartDataTable
     *
     * @return array
     */
    public function getConsecutiveDownsData(array $options, $table = 'consecutive_downs_binance', $chartDataTable = 'chart_data_binance')
    {
        $sql[] = "SELECT 
                *,
                 (SELECT SUM(volume)/90 FROM $chartDataTable AS cd WHERE cd.date >= CURRENT_TIMESTAMP - INTERVAL 90 DAY AND cd.market = cdowns.market) AS sumVolume
              FROM $table AS cdowns";

        if (!empty($options)) {
            $sql[] = "WHERE";
        }

        $where = [];
        if (!empty($options['market'])) {
            $where[] = "market = '{$options['market']}'";
        } else {
            $where[] = "market != 'BTCUSDT'";
        }

        if (!empty($options['interval']) && is_numeric($options['interval'])) {
            $where[] = "period = {$options['interval']}";
        } elseif (!empty($options['interval'])) {
            $where[] = "period = '{$options['interval']}'";
        }

        if (!empty($options['xHours'])) {
            $where[] = "x_hours = {$options['xHours']}";
        }

        if (!empty($options['consecutiveDowns'])) {
            $where[] = "consecutive_downs = {$options['consecutiveDowns']}";
        }

        if (!empty($options['percentageGoingDownGreaterThanOrEqualTo'])) {
            $where[] = "percentage_going_down >= {$options['percentageGoingDownGreaterThanOrEqualTo']}";
        }

        if (!empty($options['date_from']) && !empty($options['date_to'])) {
            $where[] = "date >= '{$options['date_from']}'";
            $where[] = "date <= '{$options['date_to']}'";
        }

        if (!empty($where)) {
            $sql[] = implode(' AND ', $where);
        }

        // Embedded select
        // sumVolume is not a table existing column
        if (!empty($options['sumVolume'])) {
            $sql[] = "HAVING sumVolume > {$options['sumVolume']}";
        }
        $sql[] = "ORDER BY x_hours, date ASC";

        if (!empty($options['limit'])) {
            $sql[] = "LIMIT " . $options['limit'] . ";";
        } elseif (!isset($options['limit']) || (isset($options['limit']) && $options['limit'])) {
            $sql[] = "LIMIT 2000;";
        }

        $downs = $this->db->execute(implode(' ', $sql));

        $data = [];
        foreach ($downs as $item) {
            $data[] = $item;
        }

        return $data;

    }

    /**
     * @param string $table
     *
     * @return string
     */
    public function getPrepareInsertSql($table = 'consecutive_downs')
    {
        return $sql = "INSERT IGNORE INTO $table 
            (`market`, `period`, `date`, 
            `open`, `volume`, `sum_volume_going_down`, `percentage_going_down`,
            `high_after_x_hours`, `low_after_x_hours`, `close_after_x_hours`, `high_date_after_x_hours`, `low_date_after_x_hours`, `close_date_after_x_hours`,
            `highest_percentage`, `lowest_percentage`,
            `highest_volume_after_x_hours`, `highest_volume_date_after_x_hours`, `consecutive_downs`, `x_hours`) VALUES ";
    }

    /**
     * @param int    $months
     * @param string $table
     */
    public function deleteOldData($months, $table = 'consecutive_downs')
    {
        $sql = "DELETE FROM $table WHERE date < CURRENT_TIMESTAMP - INTERVAL $months MONTH;";
        $this->db->execute($sql);
    }

}