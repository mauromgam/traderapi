<?php
require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/../APIs/APIPoloniex.php';
require_once __DIR__ . '/TraderPoloniex.php';
require_once __DIR__ . '/ChartData.php';
require_once __DIR__ . '/../Utils/Messages.php';
require_once __DIR__ . '/../SmsAlert/SmsAlert.php';

echo sprintf(Messages::STARTING_TESTS, date('Y-m-d H:i:s'));

$script = 'A';
if (!empty($argv[1]) && in_array($argv[1], ['A','B','C'])) {
    $script = $argv[1];
}

$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tpolApiKey\n";
    echo "\tpolApiSecret\n";
    echo "config/config.php\n\n";
    exit(-1);
}


/**
 * In real life, the date threshold must not be more than ($numberOfDowns + 1) * 5min
 * Ex: if $numberOfDowns = 10, then date threshold should be  11 * 5 = 55min
 */
$numberOfDowns = 10;
$xHours = 24;

// UPDATE USER BALANCES
echo Messages::UPDATING_USER_BALANCES;
exec("php ../importData/poloniex/import_my_balances.php 2000-01-01");


$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$api = new APIPoloniex($key, $apisecret);
$trader = new TraderPoloniex($db, $api);
$smsAlert = new SmsAlert($botConfig['smsApi'], $botConfig['smsSecret'], null, $botConfig['smsTo'], $db);
$chartData = new ChartData($db);
$now = date('Y-m-d H:i:s');


// CHECK IF THERE IS ANY OPEN BUY ORDER
$openOrder = $trader->getOpenOrders(TraderPoloniex::BUY);

if ($openOrder['rate'] == 0) {
    echo Messages::NO_OPEN_ORDER_FOUND;
    die(1);
}

// CHECK IF AVAILABLE BALANCE IS GREATER THAN 0.00000010
$coin = str_replace('BTC_', '', $openOrder['market']);
$balance = $trader->getUserBalance($coin);

if (false && $balance['available'] < 0.00000010) {
    echo Messages::OPEN_ORDER_WITH_BALANCE_ZERO;
    $message = Messages::OPEN_ORDER_WITH_BALANCE_ZERO . " openOrderId: {$openOrder['id']}";
    $smsAlert->setPair($openOrder['market']);
    $smsAlert->sendCustomAlert($now, $message);
    die(1);
}


// GET TICKER FOR THE CURRENT COIN WITH OPEN ORDER
$ticker = $api->get_ticker($openOrder['market']);
$lastPrice = $ticker['last'];

// GET THE LAST 5 MINUTES LOWEST AND HIGHEST VALUES
$options = [
    'market' => $openOrder['market'],
    'interval' => 300,
    'limit' => 1,
    'order' => 'date DESC'
];
$dataChart = $chartData->getChartData($options, true);
$candle = $dataChart['data'][0];

$timePast = date('H', $now - $openOrder['date']);

if ($trader->trailStopLossPoloniex($lastPrice, $candle['low'], $highestHigh, $previousHighestHigh, $candle['date'], $timePast)) {

}





$lastSellDate = '1970-01-01 00:00:00';
$countSkippedDowns = 0;

$highestHigh = 0;
$previousHighestHigh = null;
foreach ($candles as $key => $candle) {
    echo $market . ' - ' . $key . "\n";
    echo sprintf("openValue:     %.8f\n", $open);


    if ($candle['high'] > $highestHigh) {
        $highestHigh = $candle['high'];
    }

    $timePast = ($key+1)/12; // 12 * 5 == 1 hour
    if ($trader->trailStopLoss($candle['low'], $highestHigh, $previousHighestHigh, $candle['date'], $timePast)) {
        $lastSellDate = $candle['date'];
        break;
    }
    $previousHighestHigh = $highestHigh;
}

if ($trader->getOpenOrders(Trader::OPEN)['rate']) {
    echo Messages::POSITION_STILL_OPENED;
    die;
}


echo sprintf(Messages::SKIPPED_DOWNS, $countSkippedDowns);