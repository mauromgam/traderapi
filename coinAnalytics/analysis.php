<?php
require_once __DIR__ . '/../APIs/APIPoloniex.php';
require_once __DIR__ . '/ChartData.php';
require_once __DIR__ . '/Indicators.php';
require_once __DIR__ . '/../SmsAlert/SmsAlert.php';
require_once __DIR__ . '/../Utils/Utils.php';
require_once __DIR__ . '/../Utils/PrintData.php';
require_once __DIR__ . '/../Utils/FileHelper.php';
require_once __DIR__ . '/../config/config.php';

$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tpolApiKey\n";
    echo "\tpolApiSecret\n";
    echo "config/config.php\n\n";
    exit(-1);
}

$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);

if (isset($argv[1])) {
    $pair = Utils::getPair($db, $argv[1]);
} elseif (isset($_GET['pair'])) {
    $pair = Utils::getPair($db, escapeshellcmd(htmlentities($_GET['pair'])));
} else {
    $pair = "USDT_BTC";
}

$api = new APIPoloniex($key, $apisecret);
$chartData = new ChartData($db, 'USDT_BTC');
$smsAlert = new SmsAlert($botConfig['smsApi'], $botConfig['smsSecret'], $pair, $botConfig['smsTo'], $db);
$indicators = new Indicators($db, $pair);

$options = [
    'market' => $pair,
    'interval' => 1800,
];
$dataChart = $chartData->getChartData($options);

$highs = $dataChart['highs'];
$lows = $dataChart['lows'];
$closes = $dataChart['closes'];
$dataChartData = $dataChart['data'];

$psar = trader_sar($highs, $lows, 0.1, 0.2);

$period = 20;
$devup = $devdn = 2;
$bbandsValues = $indicators->getBbandsValuesArray($dataChartData, $closes, $period, $devup, $devdn);

$rsi = trader_rsi($closes, 20);
//

$aroon = trader_aroon($highs, $lows, 20);
//var_dump($aroon);die;

PrintData::poloniexPrintOpenHtmlTable($pair);
foreach ($dataChartData as $key => $item) {

    $bbandsValues_ = isset($bbandsValues[$key]) ? $bbandsValues[$key] : [[],[],[]];

    $return = PrintData::poloniexPrintHtmlTableRow(
        $indicators,
        $item,
        $psar,
        $bbandsValues_,
        [
            'rsi' => $rsi,
            'period' => $period
        ],
        $key
    );

    echo $return['html'];

    $squeezeCalc = $indicators->bbandsSqueezeExists($bbandsValues_);
    $squeezeAndRsi = $squeezeCalc && $return['rsi'] < 30;

    if ($squeezeAndRsi || $return['rsi'] < 30) {
        $smsAlert->sendAlert($item['date'], $return['trend'], $squeezeCalc);
    }

}
PrintData::poloniexPrintCloseHtmlTable();
