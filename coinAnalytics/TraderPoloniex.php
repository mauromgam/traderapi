<?php
/**
 * This class implements the methods to open orders,
 * trail opened orders and sell opened orders
 *
 * PHP Version 5.6^
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */

require_once __DIR__ . "/../Db/Db.php";
require_once __DIR__ . "/../APIs/APIPoloniex.php";
require_once __DIR__ . "/../Utils/Messages.php";
require_once __DIR__ . "/Trader.php";

/**
 * Class TraderPoloniex
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */
class TraderPoloniex extends Trader
{

    /**
     * @return bool
     */
    public function openOrder()
    {
        if ($this->total > 0) {
            $this->orderNumber = $this->_getLastOrderNumber() + 1;

            $tradeHistoryId = $this->_addTradeHistory();

            $sql = "INSERT INTO `user_open_orders` 
                (`tradeHistoryId`, `market`, `orderNumber`, `type`, 
                `rate`, `amount`, `total`, 
                `date`, `margin`, `status`, `script`)
                VALUES
                    ($tradeHistoryId, '{$this->market}', {$this->orderNumber}, '{$this->type}', 
                    {$this->rate}, {$this->amount}, {$this->total}, 
                    '{$this->date}', {$this->margin}, '{$this->status}', '{$this->script}');";
            $this->db->execute($sql);
            $recentOpenedOrderId = $this->db->getActiveConnection()->insert_id;

            if ($this->type == self::BUY) {

                // update local user balance
                $available = $this->getUserBalance($coin = 'BTC')['available'];
                $available = $available - $this->total;

                echo "\n\nOn Orders: $this->total\n";
                echo "Available: $available\n";
                echo "Total: {$this->total}\n\n";

                $this->_updateUserBalance($this->total, 'BTC', $available);

                // OPEN BUY ORDER ON POLONIEX
                $return = $this->api->buy($this->market, $this->rate, $this->amount);

                var_dump($return);

                if (isset($return['error'])) {

                } elseif (isset($return['orderNumber'])) {

                    // AFTER OPENING THE POLONIEX ORDER, UPDATE THE LOCAL ORDER
                    $this->_updateOpenBuyPoloniexOrderNumber($recentOpenedOrderId, $return['orderNumber']);

                } else {

                }

                sleep(1);
                exec('php ../importData/poloniex/import_my_balances.php 2000-01-01 > ../logs/myBalances.txt 2>&1 & echo $!');

                sleep(1);
                exec('php ../importData/poloniex/import_my_open_orders.php > ../logs/myOpenOrders.txt 2>&1 & echo $!');

                sleep(1);
                exec('php ../importData/poloniex/import_my_trade_history.php > ../logs/myTradeHistory.txt 2>&1 & echo $!');

            } elseif ($this->type == self::SELL) {

                // OPEN SELL ORDER ON POLONIEX
                $this->api->sell($this->market, $this->rate, $this->total);

            }

            return true;
        } else {
            echo Messages::BALANCE_ZERO;
            return false;
        }
    }

    /**
     * Updates local Open Order record with Poloniex order number
     *
     * @param int $orderId
     * @param int $orderNumber
     *
     * @return void
     */
    protected function _updateOpenBuyPoloniexOrderNumber($orderId, $orderNumber)
    {
        $sql = "UPDATE user_open_orders SET 
               orderNumber = $orderNumber
            WHERE 
              id = $orderId;";
        $this->db->execute($sql);
    }

    /**
     * @param string $coin
     * @param double $limit
     * @param string $table
     *
     * @return array
     */
    public function getUserBalance($coin = 'BTC', $limit = null, $table = 'user_balances_poloniex')
    {
        return parent::getUserBalance($coin, $limit, $table);
    }

    /**
     * Poloniex version must be called every 3 seconds
     *  - get the ticker for the market that holds the coins
     *
     * This method will open a trail stop loss position
     * What it will do is to open a SELL order for the coin that has
     * a BUY order with status equals to OPEN
     *
     * @param double $lastValue
     * @param double $lastLowValue
     * @param double $highestHigh
     * @param double $lastHighestHigh
     * @param string $date
     * @param float  $timePast
     *
     * @return bool
     */
    public function trailStopLossPoloniex($lastValue, $lastLowValue, $highestHigh, $lastHighestHigh, $date, $timePast)
    {
        $openBuyOrder = $this->getOpenOrders(self::BUY);

        if ($openBuyOrder['rate'] > 0) {
            $highPercentage = (($highestHigh/$openBuyOrder['rate'])-1);

            // Get existing stop loss
            $curStopLoss = $this->_getCurrentTrailStopLoss($openBuyOrder['tradeHistoryId']);
            $newRate = $curStopLoss = (double)$curStopLoss['stop'];

            echo sprintf("highPercentage: %.8f\n", $highPercentage * 100);

            if ($highPercentage >= 0.015) {

                echo sprintf("highestHigh: %.8f\n", $highestHigh);

                $newRate = $lastLowValue - ($lastLowValue * 0.07);
            }

            if ($this->_executeEarlyStop($lastLowValue, $openBuyOrder, $curStopLoss, $timePast)) {

                echo "Closing order after $timePast hours.\n\n";
                $this->_sellImmediately($lastLowValue, $openBuyOrder, $date);

                return true;
            }


            echo sprintf("lastLowValue: %.8f\n", $lastLowValue);
            echo sprintf("sellRate:     %.8f\n", $curStopLoss);
            echo sprintf("\nCurrent Percentage:     %.8f\n", (($lastLowValue/$openBuyOrder['rate']) - 1) * 100);
            echo sprintf("\nStopLoss Percentage:     %.8f\n", (($curStopLoss/$openBuyOrder['rate']) - 1) * 100);

            echo sprintf("\nStatus:     %s\n", ($lastLowValue <= $curStopLoss && $curStopLoss > 0) ? 'SELL' : 'HOLD');

            if ($lastLowValue <= $curStopLoss && $curStopLoss > 0) {

                echo "\nLow value lower than stop loss\n";
                echo sprintf("\tLow Value: %.8f\n", $lastLowValue);
                echo sprintf("\tStop Loss: %.8f\n\n", $curStopLoss);
                $this->_sellImmediately($lastLowValue, $openBuyOrder, $date);

                return true;
            }

            // Remove existing open stop loss order and set up a new one
            $this->_setStopLoss($newRate, $openBuyOrder, $date);

        }

        return false;
    }

    /**
     * TODO: Implement version to work with Poloniex
     *
     * @param double $lastValue
     * @param array  $openSellOrder
     * @param double $newTotal
     * @param array  $openBuyOrder
     * @param string $date
     *
     * @return void
     */
    protected function _closeOpenOrder($lastValue, $openSellOrder, $newTotal, $openBuyOrder, $date)
    {
        echo "\n\nPosition closed: ($date) \n";
        echo sprintf("\tRate = %.8f\n", $lastValue);
        echo sprintf("\tAmount = %.8f\n", $openSellOrder['amount']);
        echo sprintf("\tNewTotal = %.8f\n", $newTotal);
        echo "\tPercentage won = " . (($newTotal/$openBuyOrder['total'])-1)*100 . "%\n\n";

        sleep(4);

        $balance = $this->getUserBalance($coin = 'BTC');

        $onOrders = $balance['onOrders'] - $openBuyOrder['total'];

        echo "\n\nClosing...\n";
        echo "On Orders: $onOrders\n";
        echo "Available: $newTotal\n\n";
        //        sleep(3);

        $this->_updateOpenSellOrder($openSellOrder['tradeHistoryId'], $lastValue, $newTotal, self::CLOSE, $date);
        $this->_updateOpenBuyOrder($openBuyOrder['tradeHistoryId'], self::CLOSE);
        $this->_updateUserBalance($onOrders, self::COIN_BTC, $newTotal);

    }

    /**
     * TODO: Implement version to work with Poloniex
     *
     * @param double $newRate
     * @param array  $openBuyOrder
     * @param string $date
     *
     * @return void
     */
    protected function _setStopLoss($newRate, $openBuyOrder, $date)
    {
        if ($newRate == 0) {
            return;
        }

        // Remove existing open stop loss order
        $openTradeHistoryId = $openBuyOrder['tradeHistoryId'];
        if ($this->_removeExistingTrailStopLoss($newRate, $openTradeHistoryId)) {

            echo "Setting stop loss...\n";

            $sql = "INSERT INTO `trail_stop_loss` 
                (`market`, `date`, `stop`, `open`, `tradeHistoryId`)
                VALUES
                    ('{$this->market}', '$date', $newRate, {$openBuyOrder['rate']}, {$openBuyOrder['tradeHistoryId']});";

            $this->db->execute($sql);

        }
    }

    /**
     * TODO: Implement version to work with Poloniex
     *
     * @param double $rate
     * @param array  $openBuyOrder
     * @param string $date
     *
     * @return void
     */
    protected function _sellImmediately($rate, $openBuyOrder, $date)
    {

        // Open Sell order with minimum value to sell the position asap
        // TODO: when trading live, rate must be 0.0000000001 - this will secure the position close
        $data = [
            'market' => $this->market,
            'rate' => $rate,
            'amount' => $openBuyOrder['amount'],
            'date' => $date,
            'margin' => 1,
            'type' => self::SELL,
            'status' => self::OPEN,
            'script' => $this->script
        ];

        $newTrader = new self($this->db, $this->api);
        $newTrader->setData($data);

        echo "\n\nSetting Sell Order...\n";
        $newTrader->openOrder();

        $openSellOrder = $this->getOpenOrders(self::SELL);

        echo "Closing Sell Order...\n";
        $this->_closeOpenOrder($rate, $openSellOrder, $newTrader->total, $openBuyOrder, $date);

    }

    /**
     * This function will detect if an order was set up but never executed
     *
     * @return bool
     */
    public function detectNeverExecutedOpenOrders()
    {

        $openOrders = $this->api->get_all_open_orders();


        return $results->num_rows > 0;
    }
}