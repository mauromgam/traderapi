<?php
/**
 * This class implements the methods to open orders,
 * trail opened orders and sell opened orders
 *
 * PHP Version 5.6^
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */

require_once __DIR__ . "/../Db/Db.php";
require_once __DIR__ . "/../APIs/APIPoloniex.php";
require_once __DIR__ . "/../APIs/APIBinance.php";
require_once __DIR__ . "/../Utils/Messages.php";

/**
 * Class Trader
 *
 * @author Mauro Marinho <mauromgam@gmail.com>
 */
class Trader
{
    protected $db;
    protected $api;
    protected $market;
    protected $orderNumber;
    protected $type;
    protected $rate;
    protected $amount;
    protected $total;
    protected $date;
    protected $margin;
    protected $status;
    protected $script;
    protected $trailingStopLossPercentage;

    const COIN_BTC = 'BTC';
    const BUY = 'BUY';
    const SELL = 'SELL';
    const OPEN = 'OPEN';
    const CLOSE = 'CLOSE';
    const DIVISOR = 1;

    /**
     * ChartData constructor.
     *
     * @param Db $db Database object
     * @param BaseAPI $api
     * @param float $trailingStopLossPercentage
     */
    public function __construct(Db $db, BaseAPI $api, $trailingStopLossPercentage)
    {
        $this->db = $db;
        $this->api = $api;
        $this->trailingStopLossPercentage = $trailingStopLossPercentage;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Set up buy order
     *
     * @param array $data
     *
     * @return void
     */
    public function setData($data)
    {
        $this->market = isset($data['market']) ? $data['market'] : null;
        $this->type = isset($data['type']) ? $data['type'] : self::BUY;
        $this->rate = isset($data['rate']) ? $data['rate'] : null;
        $this->amount = isset($data['amount']) ? $data['amount'] : null;
        $this->total = isset($data['total']) ? $data['total'] : null;
        $this->date = isset($data['date']) ? $data['date'] : null;
        $this->margin = isset($data['margin']) ? $data['margin'] : 1;
        $this->status = isset($data['status']) ? $data['status'] : null;
        $this->script = isset($data['script']) ? $data['script'] : null;

        if ($this->rate && $this->type == self::BUY) {
            $this->amount = ($this->total / $this->rate);
        } elseif ($this->type == self::SELL) {
            $this->total = ($this->amount * $this->rate);
        }
    }

    /**
     * @return bool
     */
    public function openOrder()
    {
        if ($this->total > 0) {
            $this->orderNumber = $this->_getLastOrderNumber() + 1;

            $tradeHistoryId = $this->_addTradeHistory();

            $sql = "INSERT INTO `user_open_orders` 
                (`tradeHistoryId`, `market`, `orderNumber`, `type`, 
                `rate`, `amount`, `total`, 
                `date`, `margin`, `status`, `script`)
                VALUES
                    ($tradeHistoryId, '{$this->market}', {$this->orderNumber}, '{$this->type}', 
                    {$this->rate}, {$this->amount}, {$this->total}, 
                    '{$this->date}', {$this->margin}, '{$this->status}', '{$this->script}');";

            $this->db->execute($sql);

            if ($this->type == self::BUY) {
                $available = $this->getUserBalance($coin = 'BTC')['available'];
                $available = $available - $this->total;

                echo "\n\nOn Orders: $this->total\n";
                echo "Available: $available\n\n";
                // sleep(3);

                $this->_updateUserBalance($this->total, 'BTC', $available);
            }

            return true;
        } else {
            echo Messages::BALANCE_ZERO;
            return false;
        }
    }

    /**
     * @return int
     */
    protected function _getLastOrderNumber()
    {
        $sql = "SELECT MAX(orderNumber) AS orderNumber FROM user_open_orders;";
        $result = $this->db->execute($sql);

        foreach ($result as $item) {
            return (int)$item['orderNumber'];
        }

        return 0;
    }

    /**
     * @param string $coin
     * @param double $limit
     * @param string $table
     *
     * @return array
     */
    public function getUserBalance($coin = 'BTC', $limit = null, $table = 'user_balances')
    {
        $sql = "SELECT available, onOrders, btcValue FROM $table WHERE coin = '$coin';";
        $results = $this->db->execute($sql);
        $results = $results->fetch_assoc();

        $total = (double)$results['available'];
        if ($limit && $total > $limit) {
            $diff = (double)$results['available'] - $limit;
            $total = ((double)$results['available'] - $diff) / self::DIVISOR;
        }

        return [
            'available' => $total,
            'onOrders' => (double)$results['onOrders'],
            'btcValue' => (double)$results['btcValue']
        ];
    }

    /**
     * This method will update the user balance whenever a BUY position is opened or closed
     *
     * @param double $onOrders
     * @param string $coin
     * @param double $available
     *
     * @return void
     */
    protected function _updateUserBalance($onOrders, $coin = 'BTC', $available = null)
    {
        if ($this->type == self::SELL) {
            return;
        }

        $sql = "UPDATE user_balances SET 
              %s 
            WHERE coin = '$coin';";

        $set[] = "onOrders = $onOrders";
        if ($available) {
            $set[] = "available = $available";
            $set[] = "btcValue = $available";
        }

        $sql = sprintf($sql, implode(',', $set));

        $this->db->execute($sql);
    }

    /**
     * @return mixed
     */
    protected function _addTradeHistory()
    {
        // TEMPORARY - must be used only for local tests
        // The 2 values returned from the database must come from the API
        $sql = "SELECT MAX(globalTradeID) + 1 AS globalTradeID, MAX(tradeID) + 1 AS tradeID FROM trade_history;";
        $result = $this->db->execute($sql);
        $result = $result->fetch_assoc();
        $globalTradeID = (int)$result['globalTradeID'];
        $tradeID = (int)$result['tradeID'];

        $sql = "INSERT INTO `trade_history` (`market`, `globalTradeID`, `tradeID`, 
            `date`, `type`, `rate`, `amount`, `total`, `script`)
            VALUES
                ('{$this->market}', $globalTradeID, $tradeID, 
                '{$this->date}', '{$this->type}', {$this->rate}, 
                {$this->amount}, {$this->total}, '{$this->script}');";
        $this->db->execute($sql);

        return $this->db->getActiveConnection()->insert_id;
    }

    /**
     * @param string $type
     * @return array
     */
    public function getOpenOrders($type)
    {
        $sql = "SELECT id, market, rate, amount, total, tradeHistoryId, orderNumber, date 
          FROM user_open_orders 
          WHERE ";
        if ($this->market) {
            $where[] = "market = '{$this->market}'";
        }

        if ($this->script) {
            $where[] = "script = '{$this->script}'";
        }

        $where[] = "status = 'OPEN' AND type = '$type';";

        $where = implode(' AND ', $where);
        $sql = $sql . $where;

        $results = $this->db->execute($sql);

        $openOrder = $results->fetch_assoc();
        $openOrder['rate'] = isset($openOrder['rate']) ? (double)$openOrder['rate'] : 0;
        $openOrder['amount'] = isset($openOrder['amount']) ? (double)$openOrder['amount'] : 0;
        $openOrder['total'] = isset($openOrder['total']) ? (double)$openOrder['total'] : 0;
        $openOrder['tradeHistoryId'] = isset($openOrder['tradeHistoryId']) ? (int)$openOrder['tradeHistoryId'] : 0;

        return $openOrder;
    }

    /**
     * This method will open a trail stop loss position
     * What it will do is to open a SELL order for the coin that has
     * a BUY order with status equals to OPEN
     *
     * @param double $lastLowValue
     * @param double $highestHigh
     * @param double $lastHighestHigh
     * @param string $date
     * @param float  $timePast
     *
     * @return bool
     */
    public function trailStopLoss($lastLowValue, $highestHigh, $lastHighestHigh, $date, $timePast)
    {
        $openBuyOrder = $this->getOpenOrders(self::BUY);

        if ($openBuyOrder['rate'] > 0) {
            $highPercentage = (($highestHigh/$openBuyOrder['rate'])-1);

            // Get existing stop loss
            $curStopLoss = $this->_getCurrentTrailStopLoss($openBuyOrder['tradeHistoryId']);
            $newRate = $curStopLoss = (double)$curStopLoss['stop'];

            echo sprintf("highPercentage: %.8f\n", $highPercentage * 100);

            if ($highPercentage >= 0.03 && $lastHighestHigh < $highestHigh) {

                echo sprintf("highestHigh: %.8f\n", $highestHigh);

                $newRate = $lastLowValue - ($lastLowValue * $this->trailingStopLossPercentage);
            }

            if ($this->_executeEarlyStop($lastLowValue, $openBuyOrder, $curStopLoss, $timePast)) {

                echo "\n\e[0;31;40m Closing order after $timePast hours.\n\n \e[3;37;40m";
                $this->_sellImmediately($lastLowValue, $openBuyOrder, $date);

                return true;
            }


            echo sprintf("lastLowValue: %.8f\n", $lastLowValue);
            echo sprintf("sellRate:     %.8f\n", $curStopLoss);
            echo sprintf("\nCurrent Percentage:     %.8f\n", (($lastLowValue/$openBuyOrder['rate']) - 1) * 100);
            echo sprintf("\nStopLoss Percentage:     %.8f\n", (($curStopLoss/$openBuyOrder['rate']) - 1) * 100);

            echo sprintf("\nStatus:     %s\n", ($lastLowValue <= $curStopLoss && $curStopLoss > 0) ? 'SELL' : 'HOLD');

            if ($lastLowValue <= $curStopLoss && $curStopLoss > 0) {

                echo "\e[0;33;40m \nLow value lower than stop loss\n \e[3;37;40m";
                echo sprintf("\tLow Value: %.8f\n", $lastLowValue);
                echo sprintf("\tStop Loss: %.8f\n\n", $curStopLoss);
                $this->_sellImmediately($lastLowValue, $openBuyOrder, $date);

                return true;
            }

            // Remove existing open stop loss order and set up a new one
            $this->_setStopLoss($newRate, $openBuyOrder, $date);

        }

        return false;
    }

    /**
     * This method will delete the OPEN SELL position if the newRate > curRate
     *
     * @param double $newRate
     * @param int    $openTradeHistoryId
     *
     * @return bool
     */
    protected function _removeExistingTrailStopLoss($newRate, $openTradeHistoryId)
    {

        if ($openTradeHistoryId) {

            $curRate = $this->_getCurrentTrailStopLoss($openTradeHistoryId);

            echo sprintf("New rate: %.8f CurRate: %.8f\n\n", $newRate, $curRate['stop']);
            // usleep(500000);

            if ($newRate < $curRate['stop']) {
                return false;
            }

            $sql = "DELETE FROM trail_stop_loss WHERE tradeHistoryId = $openTradeHistoryId;";
            $this->db->execute($sql);

            return true;
        }

        return true;
    }

    /**
     * @param int    $tradeHistoryId
     * @param string $status
     *
     * @return void
     */
    protected function _updateOpenBuyOrder($tradeHistoryId, $status)
    {
        $sql = "UPDATE user_open_orders SET 
               status = '$status'
            WHERE 
              tradeHistoryId = $tradeHistoryId
              AND type = 'BUY'
              AND status = 'OPEN';";
        $this->db->execute($sql);
    }

    /**
     * @param int    $tradeHistoryId
     * @param double $lastValue
     * @param double $newTotal
     * @param string $status
     * @param string $date
     *
     * @return void
     */
    protected function _updateOpenSellOrder($tradeHistoryId, $lastValue, $newTotal, $status, $date)
    {
        $sql = "UPDATE user_open_orders SET 
               rate = $lastValue,
               total = $newTotal,
               status = '$status',
               date = '$date'
            WHERE 
              tradeHistoryId = $tradeHistoryId
              AND type = 'SELL'
              AND status = 'OPEN';";
        $this->db->execute($sql);

        $sql = "UPDATE trade_history SET 
               rate = $lastValue,
               total = $newTotal,
               date = '$date'
            WHERE 
              id = $tradeHistoryId
              AND type = 'SELL';";
        $this->db->execute($sql);
    }

    /**
     * @param double $lastValue
     * @param array  $openSellOrder
     * @param double $newTotal
     * @param array  $openBuyOrder
     * @param string $date
     *
     * @return void
     */
    protected function _closeOpenOrder($lastValue, $openSellOrder, $newTotal, $openBuyOrder, $date)
    {
        echo "\n\nPosition closed: ($date) \n";
        echo sprintf("\tRate = %.8f\n", $lastValue);
        echo sprintf("\tAmount = %.8f\n", $openSellOrder['amount']);
        echo sprintf("\tNewTotal = %.8f\n", $newTotal);
        echo "\tPercentage won = " . (($newTotal/$openBuyOrder['total'])-1)*100 . "%\n\n";

//        sleep(4);

        $balance = $this->getUserBalance($coin = 'BTC');

        $onOrders = $balance['onOrders'] - $openBuyOrder['total'];

        echo "\n\nClosing...\n";
        echo "On Orders: $onOrders\n";
        echo "Available: $newTotal\n\n";
        //        sleep(3);

        $this->_updateOpenSellOrder($openSellOrder['tradeHistoryId'], $lastValue, $newTotal, self::CLOSE, $date);
        $this->_updateOpenBuyOrder($openBuyOrder['tradeHistoryId'], self::CLOSE);
        $this->_updateUserBalance($onOrders, self::COIN_BTC, $newTotal);

    }

    /**
     * Early stop will only be triggered if no stop loss was set up at any point
     *
     * @param double $lastLowValue
     * @param array  $openBuyOrder
     * @param double $curStopLoss
     * @param int    $timePast
     *
     * @return bool
     */
    protected function _executeEarlyStop($lastLowValue, $openBuyOrder, $curStopLoss, $timePast)
    {
        $curLowPercentage = ($lastLowValue/$openBuyOrder['rate'])-1;

        if ($timePast >= 24) { // past 24 hours or more

            return true;

        } elseif ($timePast >= 20 && $curStopLoss == 0) { // past 20 hours

            if ($curLowPercentage > -0.01) {
                return true;
            }

        } elseif ($timePast >= 15 && $curStopLoss == 0) { // past 15 hours

            if ($curLowPercentage > -0.005) {
                return true;
            }

        } elseif ($timePast >= 5 && $curStopLoss == 0) { // past 5 hours

            if ($curLowPercentage > 0.01) {
                return true;
            }

//        } elseif ($curLowPercentage > 0.15) {
//            return true;
        }

        return false;
    }

    /**
     * @param double $newRate
     * @param array  $openBuyOrder
     * @param string $date
     *
     * @return void
     */
    protected function _setStopLoss($newRate, $openBuyOrder, $date)
    {
        if ($newRate == 0) {
            return;
        }

        // Remove existing open stop loss order
        $openTradeHistoryId = $openBuyOrder['tradeHistoryId'];
        if ($this->_removeExistingTrailStopLoss($newRate, $openTradeHistoryId)) {

            echo "Setting stop loss...\n";

            $sql = "INSERT INTO `trail_stop_loss` 
                (`market`, `date`, `stop`, `open`, `tradeHistoryId`)
                VALUES
                    ('{$this->market}', '$date', $newRate, {$openBuyOrder['rate']}, {$openBuyOrder['tradeHistoryId']});";

            $this->db->execute($sql);

        }
    }

    /**
     * @param int $openTradeHistoryId
     *
     * @return array
     */
    protected function _getCurrentTrailStopLoss($openTradeHistoryId)
    {
        $sql = "SELECT * FROM trail_stop_loss WHERE tradeHistoryId = $openTradeHistoryId;";
        $result = $this->db->execute($sql);
        $curStopLoss = $result->fetch_assoc();

        $curStopLoss['market'] = isset($curStopLoss['market']) ? $curStopLoss['market'] : null;
        $curStopLoss['date'] = isset($curStopLoss['date']) ? $curStopLoss['date'] : null;
        $curStopLoss['stop'] = isset($curStopLoss['stop']) ? (double)$curStopLoss['stop'] : 0;
        $curStopLoss['open'] = isset($curStopLoss['open']) ? (double)$curStopLoss['open'] : 0;
        $curStopLoss['tradeHistoryId'] = isset($curStopLoss['tradeHistoryId']) ? $curStopLoss['tradeHistoryId'] : 0;

        return $curStopLoss;
    }

    /**
     * @param double $rate
     * @param array  $openBuyOrder
     * @param string $date
     *
     * @return void
     */
    protected function _sellImmediately($rate, $openBuyOrder, $date)
    {

        // Open Sell order with minimum value to sell the position asap
        // TODO: when trading live, rate must be 0.0000000001 - this will secure the position close
        $data = [
            'market' => $this->market,
            'rate' => $rate,
            'amount' => $openBuyOrder['amount'],
            'date' => $date,
            'margin' => 1,
            'type' => self::SELL,
            'status' => self::OPEN,
            'script' => $this->script
        ];

        $newTrader = new self($this->db, $this->api, $this->trailingStopLossPercentage);
        $newTrader->setData($data);

        echo "\n\nSetting Sell Order...\n";
        $newTrader->openOrder();

        $openSellOrder = $this->getOpenOrders(self::SELL);

        echo "Closing Sell Order...\n";
        $this->_closeOpenOrder($rate, $openSellOrder, $newTrader->total, $openBuyOrder, $date);

    }

    /**
     * @return bool
     */
    public function checkAlreadyExecutedOrders()
    {
        $sql = "SELECT id, market, rate, amount, total, tradeHistoryId 
          FROM user_open_orders 
          WHERE 
            market = '{$this->market}' 
            AND date = '{$this->date}';";
        $results = $this->db->execute($sql);

        return $results->num_rows > 0;
    }

}