<?php
require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/../APIs/APIPoloniex.php';
require_once __DIR__ . '/TraderPoloniex.php';
require_once __DIR__ . '/ConsecutiveDowns.php';
require_once __DIR__ . '/ChartData.php';
require_once __DIR__ . '/../Utils/Messages.php';

echo sprintf(Messages::STARTING_TESTS, date('Y-m-d H:i:s'));

$script = 'A';
if (!empty($argv[1]) && in_array($argv[1], ['A','B','C'])) {
    $script = $argv[1];
}

$key = $botConfig['polApiKey'];
$apisecret = $botConfig['polApiSecret'];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tpolApiKey\n";
    echo "\tpolApiSecret\n";
    echo "config/config.php\n\n";
    exit(-1);
}


/**
 * In real life, the date threshold must not be more than ($numberOfDowns + 1) * 5min
 * Ex: if $numberOfDowns = 10, then date threshold should be  11 * 5 = 55min
 */
$numberOfDowns = 10;
$xHours = 24;

// UPDATE USER BALANCES
echo Messages::UPDATING_USER_BALANCES;
exec("php ../importData/poloniex/import_my_balances.php 2000-01-01");


$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$api = new APIPoloniex($key, $apisecret);
$trader = new TraderPoloniex($db, $api);
$consecutiveDowns = new ConsecutiveDowns($db);
$chartData = new ChartData($db);


// CHECK IF ANY FALL HAPPENED WITHIN THE LAST 5 MINUTES
$now = date('Y-m-d H:i:s');
$date = new DateTime($now);
$date->sub(new DateInterval('P30D'));
$start = date_format($date, 'Y-m-d H:i:s');
$options = [
    'interval' => 300,
    'percentageGoingDownGreaterThanOrEqualTo' => 7,
    'sumVolume' => 55,
    'x_hours' => $xHours,
    'limit' => false,
    'date_from' => $start,
    'date_to' => $now,
];
$downsData = $consecutiveDowns->getConsecutiveDownsData($options);

$lastSellDate = '1970-01-01 00:00:00';
$countSkippedDowns = 0;

foreach ($downsData as $item) {
    echo $item['market'] . " - " . $item['date'] . "\n";

    $balance = $trader->getUserBalance();
    $balance = $balance['available'];

    // GET PRICE USING TICKER
    $ticker = $api->get_ticker($item['market']);
    $lowestAsk = $ticker['lowestAsk'] - ($ticker['lowestAsk']/2);
    $lowestAsk = sprintf("%.8f", $lowestAsk);

    $total = $trader->getUserBalance('BTC')['available'];

    echo sprintf("lowestAsk: %.8f\n", $lowestAsk);
    echo sprintf("total: %.8f\n", $total);

    // OPEN BUY POSITION USING THE NEXT VALUE, SEEN THAT AFTER THE CLOSE,
    // IT'S NOT POSSIBLE TO OPEN A POSITION WITH THE EXACT SAME PRICE
    $market = $item['market'];
    $date = $now;

    $data = [
        'market' => $market,
        'rate' => $lowestAsk,
        'date' => $date,
        'total' => $total,
        'type' => 'BUY',
        'status' => 'OPEN',
        'script' => $script
    ];
    $trader->setData($data);
    $newOrder = $trader->openOrder();

    break;

}