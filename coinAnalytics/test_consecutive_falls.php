<?php
require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . '/../Db/Db.php';
require_once __DIR__ . '/Trader.php';
require_once __DIR__ . '/ConsecutiveDowns.php';
require_once __DIR__ . '/ChartData.php';
require_once __DIR__ . '/../Utils/Messages.php';
require_once __DIR__ . '/../Utils/Utils.php';

echo sprintf(Messages::STARTING_TESTS, date('Y-m-d H:i:s'));

$script = 'A';
if (!empty($argv[1]) && in_array($argv[1], ['A','B','C'])) {
    $script = $argv[1];
}

$exchange = (isset($argv[2]) ? $argv[2] : 'binance');

$exchange = Utils::getExchangeTables($exchange);

$key = $botConfig[$exchange['apiKey']];
$apisecret = $botConfig[$exchange['apiSecret']];
if(empty($key) || empty($apisecret)){
    echo "Set API Keys:\n";
    echo "\tapiKey\n";
    echo "\tapiSecret\n";
    echo "config/config.php\n\n";
    exit(-1);
}


$numberOfDowns = $exchange['numberOfDowns'];
$xHours = 24;


$db = new Db($botConfig['host'], $botConfig['db_user'], $botConfig['db_pass'], $botConfig['db']);
$api = new $exchange['api']($key, $apisecret);
$trader = new Trader($db, $api, $exchange['trailingStopLoss']);
$consecutiveDowns = new ConsecutiveDowns($db);
$chartData = new ChartData($db);


$period = 'P120D';

echo "Period: $period\n";

$now = date('Y-m-d H:i:s');
$date = new DateTime($now);
$date->sub(new DateInterval($period));
$start = date_format($date, 'Y-m-d H:i:s');
$options = [
    'interval' => $exchange['period'],
    'percentageGoingDownGreaterThanOrEqualTo' => $exchange['percentageGoingDownGreaterThanOrEqualTo'],
    'sumVolume' => 55,
    'x_hours' => $xHours,
    'limit' => false,
    'date_from' => $start,
    'date_to' => $now,
    'consecutiveDowns' => $numberOfDowns
];
$downsData = $consecutiveDowns->getConsecutiveDownsData($options, $exchange['consecutive_downs'], $exchange['chart_data']);

$lastSellDate = '1970-01-01 00:00:00';
$countSkippedDowns = 0;

//$openSellOrder = $trader->getOpenOrders(Trader::SELL);
//var_dump($openSellOrder);die;

// THE WAY IT WAS BUILT, WE MUST ENSURE THAT A POSITION WILL BE OPENED AND CLOSED IN ONE CYCLE
foreach ($downsData as $item) {

    echo $item['market'] . " - " . $item['date'];

    // CHECK IF NEXT ITEM DATE IS GREATER THAN THE LAST SELL DATE
    if ($item['date'] < $lastSellDate) {
        echo sprintf(Messages::CURRENT_ITEM_DATE_LOWER_THAN_LAST_SELL_DATE, $lastSellDate);
        $countSkippedDowns++;
        continue;
    } else {
        echo "\n";
    }

    // GET FUTURE DATA TO ANALYSE WHEN TO SELL
    // STORE DATE OF WHEN THE SELL POSITION WAS CLOSED
    $endDate = new DateTime($item['date']);
    $endDate->add(new DateInterval('PT48H5M'));
    $end = date_format($endDate, 'Y-m-d H:i:s');
    $options = [
        'market' => $item['market'],
        'interval' => $exchange['period'],
        'limit' => false,
        'date_from' => $item['date'],
        'date_to' => $end,
    ];
    $dataChart = $chartData->getChartData($options, $exchange['chart_data'], true);
    $candles = $dataChart['data'];

    // OPEN BUY POSITION USING THE NEXT VALUE, SEEN THAT AFTER THE CLOSE,
    // IT'S NOT POSSIBLE TO OPEN A POSITION WITH THE EXACT SAME PRICE

    $market = $candles[0]['market'];
    $open = $candles[0]['open'];
    $date = $candles[0]['date'];

    $data = [
        'market' => $market,
        'rate' => $open,
        'date' => $date,
        'total' => (double)$trader->getUserBalance('BTC')['available'],
        'type' => 'BUY',
        'status' => 'OPEN',
        'script' => $script
    ];
    $trader->setData($data);

    if (($trader->checkAlreadyExecutedOrders())) {
        echo sprintf(Messages::COIN_ALREADY_EXECUTED, $market, $date);
        continue;
    }

    $newOrder = $trader->openOrder();

    if ($newOrder) {
        $highestHigh = 0;
        $previousHighestHigh = null;
        foreach ($candles as $key => $candle) {
            echo $market . ' - ' . $key . "\n";
            echo sprintf("openValue:     %.8f\n", $open);


            if ($candle['high'] > $highestHigh) {
                $highestHigh = $candle['high'];
            }

            $timePast = ($key+1)/12; // 12 * 5 == 1 hour
            if ($trader->trailStopLoss($candle['low'], $highestHigh, $previousHighestHigh, $candle['date'], $timePast)) {
                $lastSellDate = $candle['date'];
                break;
            }
            $previousHighestHigh = $highestHigh;
        }

        if ($trader->getOpenOrders(Trader::OPEN)['rate']) {
            echo Messages::POSITION_STILL_OPENED;
            die;
        }
    }

}

echo sprintf(Messages::SKIPPED_DOWNS, $countSkippedDowns);