<?php

class Messages
{
    const IMPORT_FUNCTION_START = "Importing %s data for %s.\n";
    const IMPORT_FUNCTION_END = "%s data completed for %s.\n\n";
    const IMPORT_FUNCTION_NO_DATA = "There is no Chart Data to import for %s.\n\n";
    const IMPORT_FUNCTION_NO_DATA2 = "There is no %s to import for %s.\n\n";
    const IMPORT_SCRIPT_START = "Importing Stock data from %s (%s)...\n";
    const IMPORT_SCRIPT_END = "Script finished - %s (%s). Total execution time: %s\n\n";
    const TRY_QUERY_DATA = "Trying to get data from API for %s\n";
    const QUERY_DATA_FAIL = "Fail... Trying again.\n";
    const DETECT_CONSECUTIVE_FALLS_START = "Processing started...\n";
    const DETECT_CONSECUTIVE_FALLS_PARTIAL_END = "Processing finished for %s.\n";
    const DETECT_CONSECUTIVE_FALLS_FOUND = "Consecutive downs of %d detected on %s.\n";
    const DETECT_CONSECUTIVE_FALLS_END = "\nScript finished. (%s)\n\n";
    const DETECT_CONSECUTIVE_FALLS_TIME_TAKEN = "Time taken to process all information: %.8f\n";
    const BALANCE_ZERO = "\n\nYour balance is 0.\n\n";
    const CURRENT_ITEM_DATE_LOWER_THAN_LAST_SELL_DATE = "Skip - Item date is lower than last sell date (%s).\n";
    const STARTING_TESTS = "Starting test at %s.\n\n";
    const SKIPPED_DOWNS = "Number of skipped downs: %d.\n";
    const COIN_ALREADY_EXECUTED = "Continuing due to already have executed this coin: %s (%s).\n\n";
    const POSITION_STILL_OPENED = "Position still opened.";
    const UPDATING_USER_BALANCES = "Updating user balances.\n";
    const NO_OPEN_ORDER_FOUND = "No open order was found.";
    const OPEN_ORDER_WITH_BALANCE_ZERO = "Open order found but balance is zero.\n";
    const GETTING_BTC_PAIRS = "Getting BTC pairs...\n";

}