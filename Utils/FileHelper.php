<?php
require_once __DIR__ . "/../Db/Db.php";


/**
 * Class FileHelper
 * @package ProcessTransactions
 */
class FileHelper
{
    protected $filePath;
    protected $db;

    /**
     * FileHelper constructor.
     * @param $filePath
     * @param Db $db
     * @throws Exception
     */
    public function __construct($filePath, Db $db)
    {
        if (empty($filePath)) {
            throw new Exception('Missing file path.');
        }

        $this->filePath = $filePath;
        $this->db = $db;

    }

    /**
     * Simply loads the CSV content and stores in the database
     *
     * 0 = Symbol
     * 1 = Name
     * 2 = Country
     *
     * As the Name might contain commas, there is a check up the the 5 position
     * so the Country value is correct
     *
     */
    public function loadStockSymbolsIntoDatabase()
    {
        echo "Reading file...\n";

        $sql = "INSERT IGNORE INTO alpha_vantage_stock_symbols (`symbol`, `name`, `country`) VALUES ";

        $fp = @fopen($this->filePath, 'r') or die("The provided file " . $this->filePath . " doesn't exist. Please check the full path and file name.\nScript terminated.\n");
        $count = 1;
        while (($line = fgets($fp))) {
            $columns = array_map('trim', explode(',', $line));

            if (isset($columns[5])) {
                $columns[2] = $columns[5];
            } elseif (isset($columns[4])) {
                $columns[2] = $columns[4];
            } elseif (isset($columns[3])) {
                $columns[2] = $columns[3];
            }

            $query[] = "('" . $columns[0] .
                "', '" . preg_replace('/[^a-z ]/i', '', $columns[1]) .
                "', '" . preg_replace('/[^a-z ]/i', '', $columns[2]) . "')";

            if ($count % 400 == 0) {
                $query = $sql . implode(',', $query);
                $this->db->execute($query);
                $query = [];
            }

            $count++;
        }

        if (!empty($query)) {
            $query = $sql . implode(',', $query);
            $this->db->execute($query);
        }

        fclose($fp);
    }

}