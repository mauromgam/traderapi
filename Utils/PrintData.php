<?php
require_once __DIR__ . "/../coinAnalytics/Indicators.php";


class PrintData
{
    /**
     * @param Indicators $indicators
     * @param $value
     * @return string
     */
    public static function getStyleForBBandsResult(Indicators $indicators, $value)
    {
        if ($value < $indicators->getBbandsValues()) {
            return "<strong style='color: red'>$value</strong>";
        }

        return $value;
    }

    public static function poloniexPrintOpenHtmlTable($pair)
    {
        echo '<table cellpadding="10px" border="1">
        <thead>
        <tr>
            <td colspan="10">' . $pair . '</td>
        </tr>
        <tr>
            <td>Date</td>
            <!--            <td>Open</td>-->
            <td>High</td>
            <td>Low</td>
            <td>Close</td>
            <!--            <td>Quote Vol</td>-->
            <!--            <td>Volume</td>-->
            <!--            <td>Weight AVG</td>-->
            <td>PSAR</td>
            <td>PSAR Trend</td>
            <td>BBAND SUB</td>
            <td>RSI</td>
        </tr>
        </thead>
        <tbody>';
    }

    public static function poloniexPrintCloseHtmlTable()
    {
        echo '
        </tbody>
        </table>';
    }

    /**
     * @param Indicators $indicators
     * @param array $item
     * @param array $psar
     * @param array $bbandsValue
     * @param array $rsiAndPeriod
     * @param $key
     *
     * <!--        <td>-->' .  $item['open'] . '<!--</td>-->
     * <!--        <td>-->' .  $item['quoteVolume'] . '<!--</td>-->
     * <!--        <td>-->' .  $item['volume'] . '<!--</td>-->
     * <!--        <td>-->' .  $item['weightedAverage'] . '<!--</td>-->
     *
     *
     * @return array
     */
    public static function poloniexPrintHtmlTableRow(Indicators $indicators, $item, $psar, $bbandsValue, $rsiAndPeriod, $key)
    {
        $psarValue = null;
        $psarTrend = null;
        $trend = null;
        if ($key > 0 && $psar[$key] >= $item['high']) {
            $psarValue = $psar[$key];
            $trend = 'DOWN';
            $psarTrend = '<span style="color: red">' . $trend . '</span>';
        } elseif ($key > 0) {
            $psarValue = $psar[$key];
            $trend = 'UP';
            $psarTrend = '<span style="color: limegreen">' . $trend . '</span>';
        }

        $upperBand = reset($bbandsValue[0]);
        $lowerBand = reset($bbandsValue[2]);

        $rsi = 0;
        if (isset($rsiAndPeriod['rsi'][$rsiAndPeriod['period'] + $key])) {
            $rsi = $rsiAndPeriod['rsi'][$rsiAndPeriod['period'] + $key];
        }

        return [
            'html' => '
                <tr>
                    <td>' . $item['date'] . '</td>
                    <td>' . $item['high'] . '</td>
                    <td>' . $item['low'] . '</td>
                    <td>' . $item['close'] . '</td>
                    <td>' . $psarValue . '</td>
                    <td>' . $psarTrend . '</td>
                    <td>
                        ' . self::getStyleForBBandsResult($indicators, $upperBand - $lowerBand) . '
                    </td>
                    <td>' . $rsi . '</td>
                </tr>',
            'trend' => $trend,
            'rsi' => $rsi
        ];

    }

    public static function printHeaderOnTerminal()
    {
        printf("%s|\t\t|%s|\t|%s|\t|%s|\t|%s|\t|%s|\t|%s|\t|%s\n",
            "DATE ----",
//    "OPEN ----",
//    "HIGH ----",
//    "LOW -----",
            "CLOSE ---",
//    "QUOTE VOL",
            "VOLUME --",
//    "WEIGHTAVG",
            "PSAR ----",
            "TREND ---",
            "BBAND UPP",
            "BBAND MID",
            "BBAND LOW"
        );
    }
    public static function printOnTerminal($item, $psar, $bbands, $key)
    {
        $psarValue = null;
        $psarTrend = null;
        if ($key > 0 && $psar[$key] >= $item['high']) {
            $psarValue = $psar[$key];
            $psarTrend = 'DOWN';
        } elseif ($key > 0) {
            $psarValue = $psar[$key];
            $psarTrend = 'UP';
        }

        printf("%s|\t|%0.8f|\t|%0.8f|\t|%0.8f|\t|%s|\t|%0.8f|\t|%0.8f|\t|%0.8f\n",
            $item['date'],
//        $item['open'],
//        $item['high'],
//        $item['low'],
            $item['close'],
//        $item['quoteVolume'],
            $item['volume'],
//        $item['weightedAverage'],
            $psarValue,
            $psarTrend,
            reset($bbands[0]),
            reset($bbands[1]),
            reset($bbands[2])
        );
    }

}