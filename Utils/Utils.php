<?php
require_once __DIR__ . "/../Db/Db.php";

class Utils
{
    /**
     * @param $message
     */
    public static function errorLog($message)
    {
        $file = __DIR__ . "/../logs/error-log.txt";
        // Open the file to get existing content
        $current = file_get_contents($file);
        // Append a new person to the file
        $current .= $message . "\n";
        // Write the contents back to the file
        file_put_contents($file, $current);
    }

    /**
     * @param $n
     * @param $closes
     * @param $currentKey
     * @return array
     */
    public static function getNPreviousCloses($n, $closes, $currentKey)
    {
        $closesNew = [];
        $total = $currentKey + $n;
        for (; $currentKey < $total; $currentKey++) {
            if (isset($closes[$currentKey])) {
                $closesNew[] = $closes[$currentKey];
            } else {
                break;
            }
        }

        return $closesNew;
    }

    /**
     * @param Db $db
     * @param $cryptoCurrency
     * @return string
     */
    public static function getPair(Db $db, $cryptoCurrency)
    {
        $sql = "SELECT market FROM chart_data WHERE market LIKE '%$cryptoCurrency%';";
        $results = $db->execute($sql);

        $pair = "USDT_BTC";
        foreach ($results as $result) {
            $pair = $result['market'];
            break;
        }

        return $pair;
    }

    public static function getExchangeTables($exchange)
    {
        switch ($exchange) {
            case 'binance':
                return [
                    'consecutive_downs' => 'consecutive_downs_binance',
                    'chart_data' => 'chart_data_binance',
                    'period' => '5m',
                    'apiKey' => 'binanceApiKey',
                    'apiSecret' => 'binanceApiSecret',
                    'api' => 'APIBinance',
                    'percentageGoingDownGreaterThanOrEqualTo' => 9,
                    'numberOfDowns' => 11,
                    'trailingStopLoss' => 0.25
                ];
            default:
                return [
                    'consecutive_downs' => 'consecutive_downs',
                    'chart_data' => 'chart_data',
                    'period' => 300,
                    'apiKey' => 'polApiKey',
                    'apiSecret' => 'polApiSecret',
                    'api' => 'APIPoloniex',
                    'percentageGoingDownGreaterThanOrEqualTo' => 7,
                    'numberOfDowns' => 10,
                    'trailingStopLoss' => 0.07
                ];
        }
    }


}